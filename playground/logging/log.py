import finesse.logging as logging
from finesse.logging import logger

logger.info("Some information that won't be seen")
logger.setLevel("INFO")
logger.info("Some information that will be seen")

logger.warning("By default no subsystems are added", subsystem=logging.OPTICS)
logger.add_subsystem(logging.OPTICS)
logger.warning("They have to be added manually", subsystem=logging.OPTICS)

mech_logger = logging.getLogger("Mechanics")
mech_logger.add_subsystem(logging.MECHANICS)
mech_logger.critical("We can also construct new instances of loggers")
mech_logger.critical("With different subsystems assigned", subsystem=logging.MECHANICS)
