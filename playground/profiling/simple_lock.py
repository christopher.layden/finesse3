import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse_legacy(
    """
# laser and EOM
l i1 1 0 n00
s s0_0 0 n00 n0
mod eo1 40k 0.3 3 pm n0 n10
s s0_1 0 n10 n1
m m1 0.9 0.1 0 n1 n2
s s1 1200 n2 n3
m m2 .9 0.01 0 n3 n4

pd1 pdh 40k 0 n1
pd circ n2
xaxis m2 phi lin 0 100 200
"""
)

model.add(
    finesse.locks.Lock(
        "z", model.pdh, model.m1.phi, -10, 1e-8, initial_only=False, additive_fb=True
    )
)

profile()
