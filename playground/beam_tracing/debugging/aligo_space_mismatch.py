from finesse.detectors import PowerDetector
from finesse.parse.legacy import KatParser
from finesse.analysis import xaxis

k = KatParser()
with open("../../aligo/design.kat", "r") as file:
    katfile = file.read()

model = k.parse(katfile)

ifo = model.model

ifo.maxtem = 6
ifo.add(PowerDetector("pd_out", ifo.BS.p4.o))


xaxis(ifo.ITMX.xbeta, 0, 100e-6, 100).plot()
