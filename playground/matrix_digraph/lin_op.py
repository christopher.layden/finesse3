# -*- coding: utf-8 -*-
"""
"""
import numpy as np
import enum
import logging

LOGGER = logging.getLogger(__name__)


class DeferredMatrixStore:
    """
    This object holds (or doesn't) a scalar, diagonal, or dense matrix.
    This allows the numerical tricks used by the KLUSimulation and solver
    to be translated to the partial GE solver.

    The casting between these things is done in the KLUMatrix of the KLU
    solver (and its memory layout), but needs to be performed by the matmul
    operator and combined-mul-add operator of this class.

    Ideally this will be a Cython thing at some point
    """

    def __init__(self, ms_type, matrix, builder=None, needs_build=True):
        """
        Takes the ms_type (if it's a scalar, diagonal, or matrix). Also takes
        the actual matrix (built however it should be)
        """
        assert ms_type in MatrixStoreTypes
        self.ms_type = ms_type
        self._matrix = matrix
        self.builder = builder
        self.needs_build = needs_build

        if __debug__:
            if ms_type == MatrixStoreTypes.DIAGONAL:
                assert len(matrix.shape) >= 1
            elif ms_type == MatrixStoreTypes.MATRIX:
                assert len(matrix.shape) >= 1

    @property
    def matrix(self):
        self.ensure_built()
        return self._matrix

    def __getitem__(self, key):
        """
        """
        return self._matrix[key]

    def __setitem__(self, key, val):
        """
        """
        self._matrix[key] = val
        return

    def ensure_built(self):
        if self.needs_build:
            self.builder()
            # side effects of the build must flag the build as done
            # this checks that the side effects were intended
            assert not self.needs_build
        return

    def matadd(self, other):
        assert isinstance(other, DeferredMatrixStore)
        self.ensure_built()
        other.ensure_built()

        if self.ms_type == MatrixStoreTypes.SCALAR:
            if other.ms_type != MatrixStoreTypes.MATRIX:
                C = self._matrix + other._matrix
                ms_type = other.ms_type
            else:
                M = np.copy(other._matrix)
                M.reshape(-1)[:: M.shape[0] + 1] = (
                    self._matrix + M.reshape(-1)[:: M.shape[0] + 1]
                )
                C = self._matrix + M
                ms_type = MatrixStoreTypes.MATRIX
        elif self.ms_type == MatrixStoreTypes.DIAGONAL:
            if other.ms_type != MatrixStoreTypes.MATRIX:
                C = self._matrix + other._matrix
                ms_type = MatrixStoreTypes.DIAGONAL
            else:
                C = np.copy(other._matrix)
                # could be faster, but doesn't assume commutativity
                C.reshape(-1)[:: C.shape[0] + 1] = (
                    self._matrix + C.reshape(-1)[:: C.shape[0] + 1]
                )
                ms_type = MatrixStoreTypes.MATRIX
        else:
            ms_type = MatrixStoreTypes.MATRIX
            if other.ms_type != MatrixStoreTypes.MATRIX:
                C = np.copy(self._matrix)
                # could be faster, but doesn't assume commutativity
                C.reshape(-1)[:: C.shape[0] + 1] = (
                    C.reshape(-1)[:: C.shape[0] + 1] + other._matrix
                )
            else:
                C = self._matrix + other._matrix
        return DeferredMatrixStore(ms_type=ms_type, matrix=C, needs_build=False,)

    def matmul(self, other):
        assert isinstance(other, DeferredMatrixStore)
        self.ensure_built()
        other.ensure_built()

        # TODO, make this broadcast properly if extra axis exist
        if self.ms_type == MatrixStoreTypes.SCALAR:
            C = self._matrix * other._matrix
            ms_type = other.ms_type
        elif self.ms_type == MatrixStoreTypes.DIAGONAL:
            if other.ms_type == MatrixStoreTypes.SCALAR:
                C = self._matrix * other._matrix
                ms_type = MatrixStoreTypes.DIAGONAL
            elif other.ms_type == MatrixStoreTypes.DIAGONAL:
                C = self._matrix * other._matrix
                ms_type = MatrixStoreTypes.DIAGONAL
            else:
                C = self._matrix.reshape(-1, 1) * other._matrix
                ms_type = MatrixStoreTypes.MATRIX
        else:
            ms_type = MatrixStoreTypes.MATRIX
            if other.ms_type == MatrixStoreTypes.SCALAR:
                C = self._matrix * other._matrix
            elif other.ms_type == MatrixStoreTypes.DIAGONAL:
                C = self._matrix * other._matrix.reshape(1, -1)
            else:
                C = self._matrix @ other._matrix
        return DeferredMatrixStore(ms_type=ms_type, matrix=C, needs_build=False,)

    def matmuladd(self, othermul, otheradd):
        # assert(isinstance(othermul, DeferredMatrixStore))
        # assert(isinstance(otheradd, DeferredMatrixStore))
        # othermul.ensure_built()
        # otheradd.ensure_built()
        # TODO, actually implement this with the proper fast operation
        # in comments above
        return self.matmul(othermul).matadd(otheradd)

    def matinv(self):
        if self.ms_type == MatrixStoreTypes.SCALAR:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.SCALAR,
                matrix=1 / self._matrix,
                needs_build=False,
            )
        elif self.ms_type == MatrixStoreTypes.DIAGONAL:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.DIAGONAL,
                matrix=1 / self._matrix,
                needs_build=False,
            )
        else:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.MATRIX,
                matrix=np.linalg.inv(self._matrix),
                needs_build=False,
            )

    def matinvN(self):
        if self.ms_type == MatrixStoreTypes.SCALAR:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.SCALAR,
                matrix=-1 / self._matrix,
                needs_build=False,
            )
        elif self.ms_type == MatrixStoreTypes.DIAGONAL:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.DIAGONAL,
                matrix=-1 / self._matrix,
                needs_build=False,
            )
        else:
            return DeferredMatrixStore(
                ms_type=MatrixStoreTypes.MATRIX,
                matrix=-np.linalg.inv(self._matrix),
                needs_build=False,
            )

    def abs_sq(self):
        self.ensure_built()
        return self._matrix.real ** 2 + self._matrix.imag ** 2


@enum.unique
class MatrixStoreTypes(enum.Enum):
    """The spatial type of the model - i.e. either plane wave or modal based."""

    SCALAR = 0
    DIAGONAL = 1
    MATRIX = 2
