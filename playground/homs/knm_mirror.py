import numpy as np

import finesse
import finesse.components as fc
import finesse.detectors as fd
from finesse.utilities.xaxis import noxaxis

model = finesse.Model(maxtem=1)
L0_P = 1
model.add(fc.Laser("L0", P=L0_P))
model.add(fc.Mirror("M1", Rc=-2.5, xbeta=1e-6))
model.connect(model.L0.p1, model.M1.p1, L=1)
model.L0.p1.o.q = -1.4 + 0.8j

model.tag_node(model.M1.p1.o, "nREFL")

model.beam_trace()
print(model.last_trace)

out = noxaxis(model)

print(model.M1.K11)
