import numpy as np

import finesse
import pykat

finesse.plotting.init()

import matplotlib.pyplot as plt


model = finesse.parse("cavity_mismatch.kat", True)
ifo = model.model
out = model.run()
# out.plot(show=False)
ifo.print_mismatches()

# print(ifo.last_trace)


base = pykat.finesse.kat(kat_file="cavity_mismatch.kat")
base.verbose = False
out2 = base.run()
# out2.plot()

# print(out2.stdout)

# print(ifo.ITM.K11)
# print(ifo.ITM.K22)
# print(ifo.ITM.K21)
# print(ifo.ETM.K11)
# ifo.ITM.K11.plot(show=False)
# ifo.ITM.K21.plot(show=False)
# ifo.ETM.K11.plot(show=True)

assert np.allclose(np.abs(out["ad00_circ"]), np.abs(out2["ad00_circ"]))
assert np.allclose(np.abs(out["ad02_circ"]), np.abs(out2["ad02_circ"]))
assert np.allclose(np.angle(out["ad00_circ"]), np.angle(out2["ad00_circ"]))

print(f"sc gouy phase = {out['g'][0]} [F2]\t{out2['g'][0]} [F3]")

phase_offset = np.abs(
    np.angle(out["ad02_circ"], True) - np.angle(out2["ad02_circ"], True)
).mean()
print(f"02 circ phase offset between Finesse 2 and 3 = {phase_offset} deg")

fig1, (ax1c, ax2c) = plt.subplots(2, 1, sharex=True)

ax1c.semilogy(out.x1, np.abs(out2["ad00_circ"]), label="ad00_circ F2")
ax1c.semilogy(out.x1, np.abs(out["ad00_circ"]), label="ad00_circ F3")
ax1c.semilogy(out.x1, np.abs(out2["ad02_circ"]), label="ad02_circ F2")
ax1c.semilogy(out.x1, np.abs(out["ad02_circ"]), label="ad02_circ F3")
ax1c.legend()

ax1c.set_ylabel(r"Amplitude [$\sqrt{W}$]")

ax2c.plot(out.x1, np.angle(out2["ad00_circ"], True), label="ad00_circ F2")
ax2c.plot(out.x1, np.angle(out["ad00_circ"], True), label="ad00_circ F3")
ax2c.plot(out.x1, np.angle(out2["ad02_circ"], True), label="ad02_circ F2")
ax2c.plot(out.x1, np.angle(out["ad02_circ"], True), label="ad02_circ F3")
ax2c.legend()

ax2c.set_xlabel("ITM phi [deg]")
ax2c.set_ylabel("Phase [deg]")


fig2, (ax1r, ax2r) = plt.subplots(2, 1, sharex=True)

ax1r.semilogy(out.x1, np.abs(out2["ad00_refl"]), label="ad00_refl F2")
ax1r.semilogy(out.x1, np.abs(out["ad00_refl"]), label="ad00_refl F3")
ax1r.semilogy(out.x1, np.abs(out2["ad02_refl"]), label="ad02_refl F2")
ax1r.semilogy(out.x1, np.abs(out["ad02_refl"]), label="ad02_refl F3")
ax1r.legend()

ax1r.set_ylabel(r"Amplitude [$\sqrt{W}$]")

ax2r.plot(out.x1, np.angle(out2["ad00_refl"], True), label="ad00_refl F2")
ax2r.plot(out.x1, np.angle(out["ad00_refl"], True), label="ad00_refl F3")
ax2r.plot(out.x1, np.angle(out2["ad02_refl"], True), label="ad02_refl F2")
ax2r.plot(out.x1, np.angle(out["ad02_refl"], True), label="ad02_refl F3")
ax2r.legend()

ax2r.set_xlabel("ITM phi [deg]")
ax2r.set_ylabel("Phase [deg]")

plt.show()
