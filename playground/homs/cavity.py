import finesse

finesse.plotting.init()

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes even 4

cav FP ITM.p2.o

xaxis ITM.phi lin -180 180 600

pd refl ITM.p1.o
pd circ ETM.p1.i
pd trns ETM.p2.o
"""
)
model.add_all_ad(model.ETM.p1.i, f=model.L0.f.ref)

model.create_mismatch(model.ITM.p1.i, w0_mm=5, z_mm=2)
model.print_mismatches()
out = model.run()
figures = out.plot(logy=True, figsize_scale=2)
