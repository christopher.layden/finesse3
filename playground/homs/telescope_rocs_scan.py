import finesse
import finesse.analysis as analysis

finesse.configure(plotting=True)

IFO = finesse.Model()
IFO.parse(
    """
### L0 -> BS -> YARM of ET-LF

# input
l L0 P=1
s l0 L0.p1 BS.p1 L=9

# Main beam splitter at 60 deg input AOI
bs BS T=0.5 L=37.5u alpha=60
s BSsub1 BS.p3 BSAR1.p1 L=0.07478 nr=1.44963098985906
s BSsub2 BS.p4 BSAR2.p1 L=0.07478 nr=1.44963098985906
bs BSAR1 R=50u L=0 alpha=-36.6847
bs BSAR2 R=50u L=0 alpha=36.6847

# Y arm telescope
s lBS_ZM1 BS.p2 ZM1.p1 L=70
bs ZM1 T=250u L=37.5u Rc=-50
s lZM1_ZM2 ZM1.p2 ZM2.p1 L=50
bs ZM2 T=0 L=37.5u Rc=-82.5
s lZM2_ITMlens ZM2.p2 ITM_lens.p1 L=52.5

lens ITM_lens 75
s lITM_th2 ITM_lens.p2 ITMAR.p1 L=0

# Y arm input mirror
m ITMAR R=0 L=20u
s ITMsub ITMAR.p2 ITM.p1 L=0.2 nr=3.42#1.44963098985906
m ITM T=7000u L=37.5u Rc=-5580

# Y arm length
s l_arm ITM.p2 ETM.p1 L=10k

# Y arm end mirror
m ETM T=6u L=37.5u Rc=5580
s ETMsub ETM.p2 ETMAR.p1 L=0.2 nr=1.44963098985906
m ETMAR R=0 L=500u

# SRC
s l_SRC BSAR2.p3 SRM.p1 L=10

m SRM T=0.2 L=0 Rc=-9410
s SRMsub SRM.p2 SRMAR.p1 L=0.0749 nr=1.44963098985906
m SRMAR R=0 L=50n

# cavities
cav cavARM ITM.p2.o

modes maxtem=0

bp w_bs w0 BS.p2.i
gouy gsrc from_node=ITM.p1 to_node=SRM.p1

lambda 1550n
"""
)

out = analysis.x2axis(
    IFO.ZM1.Rcx, "lin", -80, -40, 100, IFO.ZM2.Rcx, "lin", -100, -50, 100,
)

out.plot()
