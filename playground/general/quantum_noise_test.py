"""
Tests the quantum noise being reflected from a simple mirror
"""
from finesse.parse import KatParser
from pykat import finesse
import numpy as np

katfile = """
    freq fs 1

    l l1 1 0 n1
    s s1 0 n1 n2
    m1 m1 0.05 0.01 0 n2 n3

    qnoised qd $fs 0 n2
    qshot qs 2*$fs 0 n2

    fsig sig l1 phase $fs 0
"""

k = KatParser()
ifo = k.parse(katfile)

carrier, signal = ifo.build()

qd = []
qs = []

with carrier, signal:
    Rs = np.linspace(0, 1, 101)
    for R in Rs:
        ifo.m1.R = R
        # Solve the carrier fields
        carrier.run()

        # Then solve the audio sidebands
        out = signal.run(carrier)
        qd.append(out["qd"][0])
        qs.append(out["qs"][0])
        # signal.M().print_rhs()
        # signal.print_matrix()

qd = np.array(qd)
qs = np.array(qs)
print("Finesse 3:")
print(qd)
print(qs)

kat = finesse.kat()
kat.parse(
    """
    l l1 1 0 n1
    s s1 0 n1 n2
    m1 m1 0.05 0.01 0 n2 n3

    qnoised qd 1 1 0 n2
    qshot qs 1 2 0 n2

    fsig sig l1 1 0

    xaxis m1 R lin 0 1 100
    yaxis abs:deg
    """
)
out = kat.run()

print("\nFinesse 2:")
print(out["qd"])
print(out["qs"])

print("Match:", np.allclose((qd, qs), (out["qd"], out["qs"]), atol=0))
