import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer

from finesse.components import Laser, Mirror
from finesse.model import Model

N = np.logspace(0, 4, 5)
times = np.zeros_like(N)

for j, n in enumerate(N):

    ifo = Model()

    ifo.homs.append('00')

    l1 = Laser('l1')
    m1 = Mirror("m1", R=0.9, T=0.1)
    m2 = Mirror("m2", R=1,   T=0)

    ifo.add(l1)
    ifo.add(m1)
    ifo.add(m2)

    ifo.connect(l1, 1, m1, 1)
    ifo.connect(m1, 2, m2, 1)

    carrier = ifo.build()

    xaxis = np.linspace(-90, 270, int(n))
    out   = np.zeros_like(xaxis, dtype=np.complex128)

    start = timer()

    with carrier:
        carrier.fill(fill_all=True)
        carrier._to_update.append(m1)

        idx_field = carrier.field(m1.nodes.n2o, 0, 0)

        for i, ifo.m1.phi in enumerate(xaxis):
            carrier.run()
            out[i] = carrier.out[idx_field]/np.sqrt(2)
            pass

    end = timer()
    times[j] = end-start
    print(n, end-start)

# plt.semilogy(xaxis, np.abs(out)**2)
# plt.ylabel("Power [W]")
#
# ax = plt.gca().twinx()
# ax.plot(xaxis, np.angle(out, True), 'r')
# ax.set_ylabel("Phase [Deg]")
#
# plt.xlabel("Tuning [Deg]")
#
# plt.tight_layout()
# plt.show()

