import finesse

CODE = """
#-----------------------------------------------------------------------
# pdh_signal.kat test file for kat 0.99
# (Error signal of the Pound-Drever-Hall signal)
#
# freise@aei.mpg.de  07.02.2005
#
#
#                      m1                               m2
#         .-----.     .-.                               .-.
#         |     |     | |      . . . . . . . . . .      | |
# --> n0  | EOM |  n1 | |  n2  .      s1         .  n3  | |
#         |     |     | |      . . . . . . . . . .      | |
#         `-----'     | |                               | |
#                     `-'                               `-'
#----------------------------------------------------------------------

m m1 0.9 0.0001 0 n1_2 n2         # mirror R=0.9 T=0.0001, phi=0
s s1 1200 n2 n3                 # space  L=1200
m m2 0.9 0.1 0 n3 n4            # mirror R=0.9 T=0.1 phi=0
attr m2 Rc 1400                 # ROC for m2 = 1400m

l i1 1 0 ni1                     # laser P=1W, f_offset=0Hz
s s00 0 ni1 n0
mod eo1 40k 0.3 3 pm n0 n1_1      # phase modulator f_mod=40kHz
s s0 0 n1_1 n1_2                       # midx=0.3 order=3

cav cavity1 m1 n2 m2 n3         # compute cavity eigenmodes
maxtem 3                        # TEM modes up to n+m=3

###### locking the cavity ############
#
# The cavity is locked with the Pound-Drever-Hall
# technique, the feedback is connected to m1.
# The resulting plot contains only the feedback
# signal of the control loop.

pd1 pdh 40k 0 n1_2                # diode for PDH signal
pd circ n2

# define error signal 'err'= Re(photo diode output)
#set err pdh re
# define control loop:
# output : feedback signal = $z
# input  : error signal = $err
# parameters:
# - loop gain = -10000
# - 'accuracy' threshold for error signal = 100n
#lock z $err -10k 100n

# connect the feedback to the input mirror:
#put* m1 phi $z                  # add $z to m1 tuning

#######################################

xaxis m2 ybeta lin 0 10u 10     # tune the angle of m2

yaxis abs
"""

model = finesse.parse(CODE, True)
ifo = model.model

ifo.add(finesse.locks.Lock("z", ifo.pdh, ifo.m1.phi, -10e3, 100e-9, True))

out = model.run()
out.plot(detectors="circ")


# import pykat
#
# base = pykat.finesse.kat(
#    kat_code=CODE + """
## define error signal 'err'= Re(photo diode output)
# set err pdh re
## define control loop:
## output : feedback signal = $z
## input  : error signal = $err
## parameters:
## - loop gain = -10000
## - 'accuracy' threshold for error signal = 100n
# lock z $err -10k 100n
#
## connect the feedback to the input mirror:
# put* m1 phi $z                  # add $z to m1 tuning
# """
# )
# out2 = base.run()
# out2.plot(detectors=['circ'])
