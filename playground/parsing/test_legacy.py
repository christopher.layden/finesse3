"""Legacy (Finesse 2) kat file parser tests."""

import pytest


def test_laser_with_phase(legacy_parser):
    """Test laser parsing with phase argument."""
    legacy_parser.parse("l l1 1 0 0 n1")
    assert legacy_parser.blocks[None]["lasers"][0] == {
        "name": "l1",
        "P": 1,
        "f": 0,
        "phase": 0,
        "node": "n1",
    }


def test_laser_without_phase(legacy_parser):
    """Test laser parsing without phase argument."""
    legacy_parser.parse("l l1 1 0 n1")
    assert legacy_parser.blocks[None]["lasers"][0] == {
        "name": "l1",
        "P": 1,
        "f": 0,
        "phase": 0,
        "node": "n1",
    }


def test_space_with_refractive_index(legacy_parser):
    """Test space parsing with refractive index argument."""
    legacy_parser.parse("s s1 1 1.45 n1 n2")
    assert legacy_parser.blocks[None]["spaces"][0] == {
        "name": "s1",
        "L": 1,
        "n": 1.45,
        "node1": "n1",
        "node2": "n2",
    }


def test_space_without_refractive_index(legacy_parser):
    """Test space parsing without refractive index argument."""
    legacy_parser.parse("s s1 1 n1 n2")
    assert legacy_parser.blocks[None]["spaces"][0] == {
        "name": "s1",
        "L": 1,
        "n": 1,
        "node1": "n1",
        "node2": "n2",
    }


def test_mirror_reflectivity_transmissivity(legacy_parser):
    """Test mirror parsing with R & T specified."""
    legacy_parser.parse("m m1 0.99 0.01 0 n1 n2")
    assert legacy_parser.blocks[None]["mirrors"][0] == {
        "name": "m1",
        "R": 0.99,
        "T": 0.01,
        "L": None,
        "phi": 0,
        "node1": "n1",
        "node2": "n2",
    }


def test_mirror_reflectivity_loss(legacy_parser):
    """Test mirror parsing with R & L specified."""
    legacy_parser.parse("m2 m1 0.99 0 0 n1 n2")
    assert legacy_parser.blocks[None]["mirrors"][0] == {
        "name": "m1",
        "R": 0.99,
        "T": None,
        "L": 0,
        "phi": 0,
        "node1": "n1",
        "node2": "n2",
    }


def test_mirror_transmissivity_loss(legacy_parser):
    """Test mirror parsing with T & L specified."""
    legacy_parser.parse("m1 m1 0.01 0 0 n1 n2")
    assert legacy_parser.blocks[None]["mirrors"][0] == {
        "name": "m1",
        "R": None,
        "T": 0.01,
        "L": 0,
        "phi": 0,
        "node1": "n1",
        "node2": "n2",
    }


def test_beamsplitter_reflectivity_transmissivity(legacy_parser):
    """Test beam splitter parsing with R & T specified."""
    legacy_parser.parse("bs bs1 0.99 0.01 0 45 n1 n2 n3 n4")
    assert legacy_parser.blocks[None]["beamsplitters"][0] == {
        "name": "bs1",
        "R": 0.99,
        "T": 0.01,
        "L": None,
        "phi": 0,
        "alpha": 45,
        "node1": "n1",
        "node2": "n2",
        "node3": "n3",
        "node4": "n4",
    }


def test_beamsplitter_reflectivity_loss(legacy_parser):
    """Test beam splitter parsing with R & L specified."""
    legacy_parser.parse("bs2 bs1 0.99 0 0 45 n1 n2 n3 n4")
    assert legacy_parser.blocks[None]["beamsplitters"][0] == {
        "name": "bs1",
        "R": 0.99,
        "T": None,
        "L": 0,
        "phi": 0,
        "alpha": 45,
        "node1": "n1",
        "node2": "n2",
        "node3": "n3",
        "node4": "n4",
    }


def test_beamsplitter_transmissivity_loss(legacy_parser):
    """Test beam splitter parsing with T & L specified."""
    legacy_parser.parse("bs1 bs1 0.01 0 0 45 n1 n2 n3 n4")
    assert legacy_parser.blocks[None]["beam_plitters"][0] == {
        "name": "bs1",
        "R": None,
        "T": 0.01,
        "L": 0,
        "phi": 0,
        "alpha": 45,
        "node1": "n1",
        "node2": "n2",
        "node3": "n3",
        "node4": "n4",
    }


def test_directional_beamsplitter(legacy_parser):
    """Test directional beam splitter parsing."""
    legacy_parser.parse("dbs dbs1 n1 n2 n3 n4")
    assert legacy_parser.blocks[None]["directional_beamsplitters"][0] == {
        "name": "dbs1",
        "node1": "n1",
        "node2": "n2",
        "node3": "n3",
        "node4": "n4",
    }


def test_modulator_without_phase(legacy_parser):
    """Test modulator parsing without phase argument."""
    legacy_parser.parse("mod EOM 100 0.1 3 am n1 n2")
    assert legacy_parser.blocks[None]["modulators"][0] == {
        "name": "EOM",
        "f": 100,
        "midx": 0.1,
        "order": 3,
        "type": "am",
        "phase": 0,
        "node1": "n1",
        "node2": "n2",
    }


def test_modulator_with_phase(legacy_parser):
    """Test modulator parsing with phase argument."""
    legacy_parser.parse("mod EOM 100 0.1 3 am 10 n1 n2")
    assert legacy_parser.blocks[None]["modulators"][0] == {
        "name": "EOM",
        "f": 100,
        "midx": 0.1,
        "order": 3,
        "type": "am",
        "phase": 10,
        "node1": "n1",
        "node2": "n2",
    }


def test_lens(legacy_parser):
    """Test lens parsing."""
    legacy_parser.parse("lens lens1 1 n1 n2")
    assert legacy_parser.blocks[None]["lenses"][0] == {
        "name": "lens1",
        "f": 1,
        "node1": "n1",
        "node2": "n2",
    }


def test_amplitude_detector_with_nm(legacy_parser):
    """Test amplitude detector parsing with n, m specified."""
    legacy_parser.parse("ad ad1 1 1 0 n1")
    assert legacy_parser.blocks[None]["amplitude_detectors"][0] == {
        "name": "ad1",
        "n": 1,
        "m": 1,
        "f": 0,
        "node": "n1",
    }


def test_amplitude_detector_without_nm(legacy_parser):
    """Test amplitude detector parsing without n, m specified."""
    legacy_parser.parse("ad ad1 0 n1")
    assert legacy_parser.blocks[None]["amplitude_detectors"][0] == {
        "name": "ad1",
        "n": None,
        "m": None,
        "f": 0,
        "node": "n1",
    }


def test_motion_detector(legacy_parser):
    """Test motion detector parsing."""
    legacy_parser.parse(
        """
        xd motion m1 z
    """
    )
    assert legacy_parser.blocks[None]["motion_detectors"][0] == {
        "name": "motion",
        "component": "m1",
        "motion": "z",
    }


def test_power_detector_pd_no_demod(legacy_parser):
    """Test power detector parsing specified as pd with no demodulation."""
    legacy_parser.parse("pd pd1 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "node": "n1",
    }


def test_power_detector_pd0_no_demod(legacy_parser):
    """Test power detector parsing specified as pd0 with no demodulation."""
    legacy_parser.parse("pd0 pd1 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "node": "n1",
    }


def test_power_detector_pd1_one_demod(legacy_parser):
    """Test power detector parsing specified as pd1 with one demodulation."""
    legacy_parser.parse("pd1 pd1 100 0 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "f0": 100,
        "phase0": 0,
        "node": "n1",
    }


def test_power_detector_pd1_one_demod_no_phase(legacy_parser):
    """Test power detector parsing specified as pd1 with one demodulation and no phase."""
    legacy_parser.parse("pd1 pd1 100 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "f0": 100,
        "node": "n1",
    }


def test_power_detector_pd2_two_demod(legacy_parser):
    """Test power detector parsing specified as pd2 with two demodulations."""
    legacy_parser.parse("pd2 pd1 100 0 200 180 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "f0": 100,
        "phase0": 0,
        "f1": 200,
        "phase1": 180,
        "node": "n1",
    }


def test_power_detector_pd2_two_demod_no_phase(legacy_parser):
    """Test power detector parsing specified as pd2 with two demodulations and no phase."""
    legacy_parser.parse("pd2 pd1 100 0 200 n1")
    assert legacy_parser.blocks[None]["power_detectors"][0] == {
        "name": "pd1",
        "f0": 100,
        "phase0": 0,
        "f1": 200,
        "node": "n1",
    }


def test_cavity(legacy_parser):
    """Test cavity parsing."""
    legacy_parser.parse("cav cav1 m1 n2 m2 n3")
    assert legacy_parser.blocks[None]["cavities"][0] == {
        "name": "cav1",
        "component1": "m1",
        "node1": "n2",
        "component2": "m2",
        "node2": "n3",
    }


def test_multiline_single_pass(legacy_parser):
    """Test multiple line parsing with a single pass."""
    legacy_parser.parse(
        """
        l l1 1 0 0 n1
        s s1 1 n1 n2
        m m1 1 0 0 n2 n3
        """
    )
    assert legacy_parser.blocks[None]["lasers"][0] == {
        "name": "l1",
        "P": 1,
        "f": 0,
        "phase": 0,
        "node": "n1",
    }
    assert legacy_parser.blocks[None]["spaces"][0] == {
        "name": "s1",
        "L": 1,
        "n": 1,
        "node1": "n1",
        "node2": "n2",
    }
    assert legacy_parser.blocks[None]["mirrors"][0] == {
        "name": "m1",
        "R": 1,
        "T": 0,
        "L": None,
        "phi": 0,
        "node1": "n2",
        "node2": "n3",
    }


def test_multiline_two_pass(legacy_parser):
    """Test multiple line parsing with two calls to parser."""
    legacy_parser.parse(
        """
        l l1 1 0 0 n1
        s s1 1 n1 n2
        """
    )
    legacy_parser.parse("m m1 1 0 0 n2 n3")
    assert legacy_parser.blocks[None]["lasers"][0] == {
        "name": "l1",
        "P": 1,
        "f": 0,
        "phase": 0,
        "node": "n1",
    }
    assert legacy_parser.blocks[None]["spaces"][0] == {
        "name": "s1",
        "L": 1,
        "n": 1,
        "node1": "n1",
        "node2": "n2",
    }
    assert legacy_parser.blocks[None]["mirrors"][0] == {
        "name": "m1",
        "R": 1,
        "T": 0,
        "L": None,
        "phi": 0,
        "node1": "n2",
        "node2": "n3",
    }


def test_constant(legacy_parser):
    """Test const parsing."""
    legacy_parser.parse(
        """
        const power 100
        const name srm
        """
    )
    assert legacy_parser.blocks[None]["constants"] == {"$power": 100, "$name": "srm"}


def test_attribute(legacy_parser):
    """Test attribute parsing."""
    legacy_parser.parse(
        """
        attr m1 Rcx 10 Rcy 9
        """
    )
    assert legacy_parser.blocks[None]["attributes"] == {"m1": [["Rcx", 10], ["Rcy", 9]]}


def test_variable(legacy_parser):
    """Test variable parsing."""
    legacy_parser.parse(
        """
        variable power 100
        """
    )
    assert legacy_parser.blocks[None]["variables"] == {"power": 100}


def test_function(legacy_parser):
    """Test function parsing."""
    legacy_parser.parse(
        """
        func f = ($x - 2) * 4
        func g = ln($x+3) / (8 * $y)
        """
    )
    assert legacy_parser.blocks[None]["functions"] == {
        "f": "($x - 2) * 4",
        "g": "ln($x+3) / (8 * $y)",
    }


def test_lock(legacy_parser):
    """Test lock command parsing."""
    legacy_parser.parse(
        """
        lock loki $DARM_err 10 1n
        lock* thor $mx1 -3 3u
        """
    )
    assert legacy_parser.blocks[None]["locks"][0] == {
        "name": "loki",
        "variable": "$DARM_err",
        "gain": 10,
        "accuracy": 1e-9,
        "starred": False,
    }
    assert legacy_parser.blocks[None]["locks"][1] == {
        "name": "thor",
        "variable": "$mx1",
        "gain": -3,
        "accuracy": 3e-6,
        "starred": True,
    }


def test_put(legacy_parser):
    """Test put command parsing."""
    legacy_parser.parse(
        """
        put l1 P $x1
        put* m1 phi $mx1
        """
    )
    assert legacy_parser.blocks[None]["puts"][0] == {
        "component": "l1",
        "parameter": "P",
        "variable": "$x1",
        "add": False,
    }
    assert legacy_parser.blocks[None]["puts"][1] == {
        "component": "m1",
        "parameter": "phi",
        "variable": "$mx1",
        "add": True,
    }


def test_set(legacy_parser):
    """Test set command parsing."""
    legacy_parser.parse(
        """
        set power l1 P
        """
    )
    assert legacy_parser.blocks[None]["sets"]["power"] == {
        "component": "l1",
        "parameter": "P",
    }


@pytest.mark.parametrize(
    "test_input,expected",
    (
        ("1", 1),
        ("31.425", 31.425),
        ("-1", -1),
        ("-64.132", -64.132),
        ("3e5", 3e5),
        ("5.3e6", 5.3e6),
        ("-5e7", -5e7),
        ("3.1e2", 3.1e2),
        ("4p", 4e-12),
        ("5.3u", 5.3e-6),
        ("6m", 6e-3),
        ("+7k", 7e3),
        ("8.5M", 8.5e6),
        ("4.1G", 4.1e9),
        ("9.999T", 9.999e12),
        ("+inf", float("+inf")),
    ),
)
def test_number(legacy_parser, test_input, expected):
    """Test number format parsing."""
    legacy_parser.parse("lens lens1 {} n1 n2".format(test_input))
    assert legacy_parser.blocks[None]["lenses"][0] == {
        "name": "lens1",
        "f": expected,
        "node1": "n1",
        "node2": "n2",
    }


def test_FTblock_single_line(legacy_parser):
    """Test FTblock parsing around a single kat file line."""
    legacy_parser.parse(
        r"""
        l l1 1 0 n1
        s s1 1 n1 n2

        %%% FTblock MIRROR
        m m1 1 0 0 n2 n3
        %%% FTend MIRROR
        """
    )
    assert legacy_parser.blocks[None]["lasers"][0] == {
        "name": "l1",
        "P": 1,
        "f": 0,
        "phase": 0,
        "node": "n1",
    }
    assert legacy_parser.blocks[None]["spaces"][0] == {
        "name": "s1",
        "n": 1,
        "L": 1,
        "node1": "n1",
        "node2": "n2",
    }
    assert legacy_parser.blocks["MIRROR"]["mirrors"][0] == {
        "name": "m1",
        "R": 1,
        "T": 0,
        "L": None,
        "phi": 0,
        "node1": "n2",
        "node2": "n3",
    }


def test_different_models(legacy_parser):
    """Test parsing of different models with single parser instance."""
    ifo1 = """
        l l1 1 0 0 n1
        %%% FTblock mirror
        s s1 1 n1 n2
        m m1 1 0 0 n2 n3
        %%% FTend mirror
        """

    ifo2 = """
        l l1 2 0 0 n1
        s s1 3 n1 n2
        m m1 4 0 0 n2 n3
        """

    # parse first ifo
    legacy_parser.parse(ifo1)

    # parse second ifo with same parser, but with reset state
    legacy_parser.reset()
    legacy_parser.parse(ifo2)
    res2a = legacy_parser.blocks

    # parse second ifo using a newly instantiated parser
    legacy_parser.reset()
    legacy_parser.parse(ifo2)
    res2b = legacy_parser.blocks

    assert res2a == res2b


def test_same_model(legacy_parser):
    """Test parsing of same model in stages with single parser instance."""
    ifo1 = """
        l l1 1 0 0 n1
        """

    ifo2 = """
        %%% FTblock mirror
        s s1 1 n1 n2
        m m1 1 0 0 n2 n3
        %%% FTend mirror
        """

    # parse first and second parts together
    legacy_parser.reset()
    legacy_parser.parse(ifo1 + ifo2)
    res1a = legacy_parser.blocks

    # parse first and second parts subsequently
    legacy_parser.reset()
    legacy_parser.parse(ifo1)
    legacy_parser.parse(ifo2)
    res1b = legacy_parser.blocks

    assert res1a == res1b
