from .integrators import (
    outer_conj_product,
    outer_conj_product_2,
    compute_map_knm_matrix_riemann_optimised,
)
from ..cymath.homs import HGModes

import numpy as np


def map_scattering_coefficients(q, max_order, x, y, Z):
    r"""Calculates the mode scattering coefficients up to some maximum mode order for a
    uniformly spaced 2-dimensional map. Essentially it is computing multiple overlap
    integrals between each input and output mode for some complex valued abberation map,
    Z.

    .. math::
        K_{abnm} = \iint^{\infty}_{-\infty} U_{nm}(x,y,q_{ix},q_{iy}) Z(x,y) U^*_{ab}(x,y,q_{ox},q_{oy}) \, dy \, dx

    - ab are the output mode indicies
    - nm are the input mode indicies

    Parameters
    ----------
    q : BeamParam
        Setting input and output complex beam parameters for overlap calcuations.
        Options are:
        - q : all input/output beam parameters are the same
        - (qx, qy) : astigmatic input/output beam parameters
        - (q_ix, q_iy, q_ox, q_oy) : general case, each x/y input/out can be different

    max_order : int
        Maximum order of mode scattering to compute up to.

    x, y : array[double]
        1-dimensional arrays specifying the x and y uniformally sampled axis for the map

    Z : array[complex]
        2-dimensional array describing the complex abberation being applied to some input
        beam. The shape of this array should be Z[Ny, Nx], where Ny and Nx are the number of
        y and x samples respectively.

    Returns
    -------
    K : array[complex]
        Coupling coefficients array with indexing (inx, outx, iny, outy)
        upto max_order input.

    Examples
    --------
    Same input and output beam parameter but computing the scattering
    from some arbitrary map:

        import finesse
        from finesse.knm.maps import map_scattering_coefficients
        import numpy as np

        maxtem = 10
        q = finesse.BeamParam(w0=1e-3, z=0)
        x = np.linspace(-4*q.w, 4*q.w, 201)
        y = np.linspace(-4*q.w, 4*q.w, 200)
        X, Y = np.meshgrid(x, y)
        # Mix of liner and quadratic phase terms to compute scattering coefficients for
        Z = np.exp(1j * 1e-6 * 2 * np.pi * (0.2*X + 0.1*Y + 1000*X**2 - 1000*Y**2) / 1064e-9)
        K = map_scattering_coefficients(q, maxtem, x, y, Z)
        print("HG 10 -> 30", K[1,3,0,0])
        print("HG 00 -> 00", K[0,0,0,0])
        print("HG 00 -> 21", K[0,2,0,1])

    Different input and output beam parameters. Here modelling mode mismatch
    between different beam parameters:

        qx1 = finesse.BeamParam(w0=1e-3, z=0)
        qy1 = qx1
        qx2 = finesse.BeamParam(w0=1.2e-3, z=0)
        qy2 = qx2

        Z = np.ones_like(Z)
        K = map_scattering_coefficients((qx1,qy1,qx2,qy2), maxtem, x, y, Z)
        print("HG 00 -> 20", K[0,2,0,0])
        print("HG 00 -> 00", K[0,0,0,0])
    """

    assert Z.shape == (y.size, x.size)  # map must be of shape (Ny, Nx)

    try:
        # Have all four qs
        qix, qiy, qox, qoy = q
    except TypeError:
        # no len option? only q given
        qix = qiy = qox = qoy = q
    except ValueError:
        # wrong size,
        try:
            # only provide (qx, qy)
            qix, qiy = q
            qox, qoy = q
        except ValueError:
            # single q value but in list
            qix = qiy = qox = qoy = q[0]

    modes = tuple(
        (
            (n, m)
            for n in range(max_order + 1)
            for m in range(max_order + 1)
            if n + m <= max_order
        )
    )

    dx = x[1] - x[0]
    dy = y[1] - y[0]

    # astigmatic = (qix.q != qiy.q) or (qox.q != qoy.q)
    mismatched = (qix.q != qox.q) or (qiy.q != qoy.q)

    iHGs = HGModes(qix, qiy, modes)
    Uni, Umi = iHGs.compute_1d_modes(x, y)

    if mismatched:
        oHGs = HGModes(qox, qoy, modes)
        Uno, Umo = oHGs.compute_1d_modes(x, y)

    Nm = max_order + 1
    Umm_ = np.zeros((Nm, Nm, y.size), dtype=np.complex128)
    Unn_ = np.zeros((Nm, Nm, x.size), dtype=np.complex128)
    tmp = np.zeros((Nm, Nm, y.size), dtype=np.complex128)
    K = np.zeros((Nm, Nm, Nm, Nm), dtype=np.complex128)

    if mismatched:
        outer_conj_product_2(Umi, Umo, Umm_)
        outer_conj_product_2(Uni, Uno, Unn_)
    else:
        outer_conj_product(Umi, Umm_)
        outer_conj_product(Uni, Unn_)

    compute_map_knm_matrix_riemann_optimised(dx * dy, Z, Unn_, Umm_, tmp, K)

    return np.transpose(K, (2, 1, 0, 3))  # change to (inx, outx, iny, outy)


def scattering_coefficients_to_KnmMatrix(modes, K):
    """Converts a 4-D scattering coefficient array into a 2D KnmMatrix object to be used
    in simulations.

    Parameters
    ----------
    modes : tuple, array
        Array of 2D modes indicies (n,m) to specify the order in which they
        appear in the returned matrix.

    K : array
        4D array of coefficients indexed with [in_x, out_x, in_y, out_y]

    Returns
    -------
    Knm : KnmMatrix
        A KnmMatrix class representing a 2D scattering matrix for the requested
        modes.

    Examples
    --------
        import finesse
        from finesse.knm.maps import map_scattering_coefficients, scattering_coefficients_to_KnmMatrix
        import numpy as np

        # compute all scatterings up to maximum TEM order
        maxtem = 3
        q = finesse.BeamParam(w0=1e-3, z=0)
        x = np.linspace(-4*q.w, 4*q.w, 201)
        y = np.linspace(-4*q.w, 4*q.w, 200)
        X, Y = np.meshgrid(x, y)
        # Mix of liner and quadratic phase terms to compute scattering coefficients for
        Z = np.exp(1j * 1e-6 * 2 * np.pi * X / 1064e-9)
        K = map_scattering_coefficients(q, maxtem, x, y, Z)
        # Generate specific ordering of coefficients into a 2D matrix that can
        # be used in simulations for propagating and array of modes.
        modes = (
            (0,0),
            (1,0),
            (2,1),
        )
        Knm = scattering_coefficients_to_KnmMatrix(modes, K)
        # Propagate some mode amplitudes
        Knm.data @ [1, 0.1, 0]
        >>> array([9.99995641e-01+2.95261180e-04j,
                   9.99986923e-02+2.95261180e-03j,
                   4.23516474e-22+2.16840434e-20j])
    """
    from finesse.knm.matrix import KnmMatrix

    Nm = len(modes)
    assert K.ndim == 4
    _modes = np.array(modes, dtype=np.int32)
    Knm = np.zeros((Nm, Nm), dtype=complex)
    for i, (n, m) in enumerate(modes):
        for o, (a, b) in enumerate(modes):
            Knm[o, i] = K[n, a, m, b]
    return KnmMatrix.from_buffer(Knm, _modes)
