"""A collection of methods to compute overlap integrals for modal scattering matrices.
Essentially this involves solving the following integral

.. math::
    K_{abnm} = \iint^{\infty}_{-\infty} U_{nm}(x,y,q_i) M(x,y) U^*_{ab}(x,y,q_o) \, dy \, dx

:math:`U_nm` is the initial modes in the basis :math:`q_i` we are converting from and
:math:`U_ab` are the target modes in a basis :math:`q_o` we are projecting into.

TODO
----
Should explore if decomposing compute_map_knm_matrix_riemann_optimised into real and imaginary
integrals might be faster. In cases where q_in == q_out then integrals are real, apart from
the map component which can be complex.
"""
import cython
import numpy as np
cimport numpy as np
from finesse.cymath cimport complex_t
from finesse.cymath.complex cimport conj, creal
from finesse.cymath.homs import HGModes
from scipy.linalg.cython_blas cimport zdotc, zgemm
from cython import nogil


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cpdef void outer_conj_product(complex_t[:,::1] U, complex_t[:,:,::1] result) except *:
    """Computes U * U^C and returns the output into the result array.
    Result array must be of shape (N,N,M) where U is shape (N,M).
    """
    cdef:
        Py_ssize_t i, j, k
        double a, b, c, d

    A = U.shape[0]
    B = U.shape[1]
    if not (result.shape[0] == result.shape[1] == A) or not (result.shape[2] == B):
        raise RuntimeError(f"result array wrong shape should be ({A,A,B})")

    for i in range(A):
        for j in range(A):
            for k in range(B):
                a = U[i, k].real
                b = U[i, k].imag
                c = U[j, k].real
                d = U[j, k].imag
                result[i, j, k] = a*c + b*d + 1j*(b*c - a*d) # U[i, k] * conj(U[j, k])


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cpdef void outer_conj_product_2(complex_t[:,::1] U1, complex_t[:,::1] U2, complex_t[:,:,::1] result) except *:
    """Computes U1 * U2**C and returns the output into the result array.
    Result array must be of shape (N,N,M) where U is shape (N,M).
    """
    cdef:
        Py_ssize_t i, j, k
        double a, b, c, d

    A = U1.shape[0]
    B = U1.shape[1]
    if not (result.shape[0] == result.shape[1] == A) or not (result.shape[2] == B):
        raise RuntimeError(f"result array wrong shape should be ({A,A,B})")

    for i in range(A):
        for j in range(A):
            for k in range(B):
                a = U1[i, k].real
                b = U1[i, k].imag
                c = U2[j, k].real
                d = U2[j, k].imag
                result[i, j, k] = a*c + b*d + 1j*(b*c - a*d) # U1[i, k] * conj(U2[j, k])


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cpdef void compute_map_knm_matrix_riemann_optimised(
            double dA,
            complex_t[:,::1] Z,
            complex_t[:,:,::1] Unn_,
            complex_t[:,:,::1] Umm_,
            complex_t[:,:,::1] tmp,
            complex_t[:,:,:,::1] result,
        ) except *:
    """Calculates a mode scattering matrix using a Riemann sum. This method
    uses an computationally optimised approach making use of fast BLAS
    functions. This requires the input modes to be specified in specific
    formats and memory layouts. What this functions computes is the following
    via a Riemann
    sum:

    .. math::
        K_{abnm} = \int^{\infty}_{-\infty}  u_{m}(y,q^y_i) u^*_{b}(y,q^y_o) \Bigg[ \int^{\infty}_{-\infty} Z(x,y) u_{n}(x,q^x_i) u^*_{a}(x,q^x_o) \, dx \Bigg] \, dy

    This integral is not actually performed to infinity, it is bound by the
    dimensions of the discretised map :math:`Z`. The map bound and uniform
    discretisation must be chosen to effciently sample the size of any of
    the beams and maximum mode order being used.

    Due to the optimised calculation method, the result indexing is not
    [a,b,n,m] but [m,a,n,b]. To convert it back to a more usable
    indexing use:

    >>> result = np.transpose(result, (2,1,0,3))

    Notes
    -----
    Nx - number of x samples
    Ny - number of y samples
    Nm - number of modes (n, m) being calculated

    Parameters
    ----------
    dA : double
        Area of discrete integral, dx * dy

    Z : array[complex]
        2D map of size [Ny, Nx]

    Unn_ : array[complex]
        3D array of size [Nm, Nm, Nx]. This should contain the
        Un(x) * Un'(x)**C products

    Umm_ : array[complex]
        3D array of size [Nm, Nm, Ny]. This should contain the
        Um(x) * Um'(x)**C products

    tmp : array[complex]
        Temporary storage that can be used to compute the dot
        products between Z and Unn_.
        Should be of size (Nm, Nm, Ny)

    result : array[complex]
        Resulting Knmnm output of size [Nm, Nm, Nm, Nm].
        IMPORTANT: Note output indexing of result K[m,a,n,b]
    """
    cdef:
        Py_ssize_t i, j, k, l,
        int Nx, Ny, Nm, inc=1, lda, ldb, ldc, M, N, K
        complex_t alpha = dA
        complex_t beta = 0
        complex_t *A
        complex_t *B
        complex_t *C
        complex_t *X
        complex_t *Y

    Nx = Z.shape[1]
    Ny = Z.shape[0]
    Nm = result.shape[0] # computing (Nm, Nm, Nm, Nm) output
    assert(result.shape[0] == result.shape[1] == result.shape[2] == result.shape[3])
    assert(Unn_.shape[0] == Unn_.shape[1] == result.shape[0])
    assert(Unn_.shape[0] == Unn_.shape[1] == result.shape[0])
    assert(Unn_.shape[2] == Nx)
    assert(Umm_.shape[2] == Ny)

    # setup blas zgemm variables for map product with x-modes
    # store this in a temporary memory and then apply the
    M = Ny
    N = Nm
    K = Nx
    lda = K
    ldb = K
    ldc = M
    A = &Z[0, 0]

    for i in range(Nm):
        B = &Unn_[i,0,0]
        C = &tmp[i,0,0]
        zgemm("T", "N",
            &M, &N, &K,
            &alpha, A, &lda, B, &ldb,
            &beta, C, &ldc
        )

    # Now we need to perform the product of the y-modes
    # with all the map and x-mode products
    M = Nm
    N = Nm
    K = Ny
    lda = Ny
    ldb = Ny
    ldc = Nm
    alpha = 1 # reset as already applied dA
    for i in range(Nm):
        for k in range(Nm):
            A = &Umm_[i,0,0]
            B = &tmp[k,0,0]
            C = &result[i,k,0,0]
            zgemm("T", "N",
                &M, &N, &K,
                &alpha, A, &lda, B, &ldb,
                &beta, C, &ldc
            )
