.. include:: /defs.hrst

.. _syntax:

=========
KatScript
=========

|Finesse| includes a built-in definition language called KatScript that provides an
alternative way to build models without using the Python :term:`API`. It is concise,
simple, and easy to use. The following contains a brief introduction to KatScript syntax
and the available instructions.

Supported :ref:`elements <elements>`, :ref:`commands <commands>` and :ref:`analyses
<analyses>` are described in their corresponding sections. There are also :ref:`command
<kat-commandindex>`, :ref:`element <kat-elementindex>` and :ref:`analysis
<kat-analysisindex>` indices for quick reference.

.. hint::

    KatScript in |Finesse| 3 is different to that of |Finesse| 2, however the :ref:`most
    common syntax <legacy_syntax>` is also supported via a separate legacy parser. There
    is also a :ref:`migration guide <migration>`.


Quick reference
===============

You can quickly get help for particular syntax in an interactive Python environment by
calling the :func:`~syntax` function:

.. jupyter-execute::

    import finesse
    print(finesse.syntax("mirror"))

From the :ref:`cli` you can also use:

.. code-block::

    kat3 syntax mirror
    kat3 syntax pd*  # Wildcards are allowed too.


A quick example
===============

KatScript models are built by specifying components, detectors, commands and other types
of statement on separate lines:

.. code-block:: katscript

    # Example of 10 W of laser power incident upon a Fabry-Perot cavity.
    l l1 P=10
    s s1 l1.p1 m1.p1
    m m1 R=0.99 T=0.01
    s s2 m1.p2 m2.p1
    m m2 R=0.99 T=0.01
    pd pd1 m2.p1.i
    modes(even, maxtem=10)

Each line above specifies a component, detector or command. Most of the statements are
formed from a series of space-delimited values; these represent `elements` of the model.
Elements always have unique names, which must be specified after the element type, which
is often, but not always, represented by a short one or two character name. The last
line is a `command` which sets some properties of the model or simulation. In this case,
it's setting the higher order modes to simulate. The arguments following the
element names are used to customise the element, such as to set the reflectivity and
transmissivity in the case of the mirrors. The comma-delimited parameters inside the
parentheses of the ``modes`` command serve a similar purpose.

Language fundamentals
=====================

Whitespace and comments
***********************

Whitespace can be simple spaces, tabs and newlines. Comments start with ``#``.
Everything including and after ``#`` on the same line is ignored by the parser.

.. code-block:: katscript

    # This is a comment!

Whitespace is sometimes important in KatScript, for instance between parameters of
:ref:`elements <element_definitions>`.

Types, names, and keywords
**************************

Component types (like :kat:element:`mirror`), names (like ``m1``) and keywords (like
``lin``) are written directly, without quotation marks:

.. code-block:: katscript

    mirror m1  # A mirror named 'm1'.

User-defined names have to start with a letter, but can otherwise contain integer
numbers and underscores.

Numbers
*******

Numbers can use integer, floating point, scientific and complex notation. The imaginary
unit is ``j``. SI prefices are also supported, but cannot be combined with scientific
notation. For example::

    # Typical number forms.
    1
    1.
    1.2
    -3.141
    6.5+2.0j

    # Scientific notation.
    1.1e7
    7.6e-2.1

    # SI prefices. Any of p, n, u, m, k, M, G, and T are supported.
    1m  # equivalent to 1e-3
    1k  # equivalent to 1e3

.. _element_definitions:

Components and detectors
========================

Component and detector definitions, collectively referred to as `elements`, mirror their
Python API signatures for the most part. Both positional and keyword arguments are
supported. Elements can be referred to using either their full name or an abbreviated
short form. They are defined using syntax of the form ``element name arg1 arg2
arg3=val3 ...``:

.. code-block:: katscript

    # Definition with positional arguments (Finesse 2 style): a mirror named "m1" with
    # reflectivity R=0.99 and transmissivity T=0.01.
    mirror m1 0.99 0.01

    # Definition with keyword arguments.
    # Note that only two of "R", "T" and "L" are required, and the third is calculated
    # from the first two. You can still specify all three, but they must all add up to
    # 1.
    mirror m2 R=0.99 T=0.01

    # You can mix positional and keyword arguments as long as they make sense. Like in
    # Python, positional arguments must always come before keyword arguments.
    mirror m3 1 0 Rc=[1, 1.1]  # Rcx and Rcy defined together.

Element definitions cannot span multiple lines.

Many components and detectors have aliases that can be used instead of longer names. For
example, ``m`` can be used instead of ``mirror``, or ``bs`` instead of ``beamsplitter``.
Available aliases are listed for each instruction in the syntax references for
:ref:`elements`, :ref:`commands`, and :ref:`analyses`.

.. _function_syntax:

Commands and analyses
=====================

Commands set some special behaviour in the model. Analyses define one or more procedures
to perform on the model defined in the script. Both commands and analyses are defined
using syntax in the form ``command(arg1, arg2, ...)``:

.. code-block:: katscript

    # Command used to set the higher order modes to simulate.
    modes(maxtem=3)

    # The `modes` command supports various different types of input. You can for example
    # specify individual modes...
    modes([[1, 0], [0, 1], [2, 0]])

    # ...or odd modes up to order 9.
    modes(odd, maxtem=9)

The arguments inside the parentheses in command and analysis definitions can span across
multiple lines, and may contain comments:

.. code-block:: katscript

    modes(
        even,
        # Set maximum order.
        maxtem=10
    )

Some analyses can contain other analyses; this is used for example to build a set of
nested procedures to perform on the model:

.. code-block:: katscript

    # Xaxis function to perform a linear sweep over the value of m1.phi.
    xaxis(m1.phi, lin, 0, 10k, 500)

    # A nested action tree.
    series(
        # First sweep m1.R...
        xaxis(m1.R, lin, 0, 1, 10),
        # ...then L0.P.
        xaxis(L0.P, lin, 10, 100, 20)
    )

Ports and nodes
===============

Some components define `ports` which act as reference points in the model to which
detectors and other components may be connected or certain properties such as beam sizes
may be defined. Ports themselves contain `nodes` which provide access to the underlying
directions. For a full explanation, see :ref:`ports`. Some brief examples are shown
below:

.. code-block:: katscript

    # Spaces connect directly to components' ports.
    l l1 P=1
    s s1 l1.p1 m1.p1 L=1
    m m1 0.99 0.01
    s s1 m1.p2 m2.p1 L=1
    m m2 0.99 0.01

    # Detectors connect to the nodes within ports, allowing the direction to detect to
    # be specified.
    pd circ m2.p1.i  # Incoming beam at port p1
    pd trans m2.p2.o  # Outgoing beam at port p2

Paths
=====

Elements and their attributes and parameters can be addressed elsewhere in models using
their `path`. Elements are simply referred to by their name; for example, a
:kat:element:`mirror` named `m1` has path ``m1``. Attribute and parameter paths are
formed by combining their owner's path with the attribute or parameter name, separated
by a ``.``; for example, a mirror's `phi` model parameter has path ``m1.phi``. Ports and
nodes are also addressable this way: the mirror's first port has path ``m1.p1`` and the
incoming node of that port has path ``m1.p1.i``. Commands and analyses do not have names
and can thus not be addressed this way.

Paths are mainly useful when specifying spaces, detectors and analyses. Spaces can be
used to connect components by referring to the paths of their ports:

.. code-block:: katscript

    m m1 R=0.99 T=0.01
    m m2 R=0.99 T=0.01
    s s1 m1.p2 m2.p1 L=10  # Connect port 2 of m1 to port 1 of m2.

Detectors can "look at" a particular port or node in a model:

.. code-block:: katscript

    l l1 P=10
    m m1 R=0.99 T=0.01
    s s1 l1.p1 m1.p1
    pd pd1 m1.p1.i  # Look at the power incident on m1 on the port 1 side
    pd pd2 m1.p2.o  # Look at the power outgoing from m1 on the port 2 side
    ad a m1.p2.o f=0

.. note::
    The syntax of |Finesse| 3 detector code has changed the order of arguments compared to version 2. The nodes are now
    specified after the name of the detector, rather than at the end of the command. This
    is because we can now allow keyword-arguments and other optional parameters which
    are easier to parse when all the required arguments are first.

Certain :ref:`analyses <analyses>` can sweep model parameters:

.. code-block:: katscript

    l l1 P=1
    m m1 R=0.99 T=0.01
    m m2 R=0.99 T=0.01
    s s1 m1.p2 m2.p1 L=10
    xaxis(l1.P, lin, 0, 10, 100)  # Sweep l1's power from 0 to 10 W over 100 points.

.. _expressions:

Expressions and references
==========================

Parameter values can contain expressions:

.. code-block:: katscript

    m m2 R=0.5 T=0.5 phi=cos(2*pi)

When used in element definitions, expressions cannot contain spaces. Spaces are allowed
when the expression is enclosed in parentheses, either inside a function or
parentheses:

.. code-block:: katscript

    # Allowed:
    l l2 P=&l1.P/10
    l l2 P=(&l1.P / 10)
    l l2 P=cos(&l1.P / 10)  # Function arguments can also have whitespace.

    # NOT allowed:
    # l l2 P=&l1.P / 10

Supported operators
*******************

The following operators are supported:

========  ==============
Operator  Description
========  ==============
``+``     Addition
``-``     Subtraction
``*``     Multiplication
``/``     Division
``//``    Floor division
``**``    Exponentiation
========  ==============

Supported expression functions
******************************

The following mathematical functions are supported:

===========  =========================
Function     Description
===========  =========================
``abs``      Absolute value
``neg``      Negation
``pos``      Positive
``conj``     Conjugate
``real``     Real part
``imag``     Imaginary part
``exp``      Exponential
``sin``      Sine
``arcsin``   Arcsine
``cos``      Cosine
``arccos``   Arccosine
``tan``      Tangent
``arctan2``  (Two-argument) arctangent
``sqrt``     Square root
``radians``  Degrees to radians
``degrees``  Radians to degrees
===========  =========================

Functions that require multiple arguments (currently only ``arctan2``) should have those
arguments separated by a comma::

    arctan2(1, 0)

References
**********

Expressions in KatScript can make references to model parameters by prefixing the
parameter's address with an ampersand (``&``). This allows the value of the target model
parameter to be used inside expressions. Since expressions can themselves be the value
of other model parameters, this lets model parameters be linked:

.. code-block:: katscript

    m m1 0.99 0.01
    m m2 R=&m1.R T=&m1.T  # m2's R and T parameters take their values from those of m1.

References can be included within larger expressions:

.. code-block:: katscript

    l l1 P=1
    l l2 P=2*&l1.P  # Set l2's power to twice that of l1.

References can also refer to other parameters within the same element:

.. code-block:: katscript

    m m1 R=1-&m1.T T=0.01  # Set R to ensure R + T = 1.

Model parameter values containing references (either directly or via expressions) get
updated on each step of simulations, allowing for a single axis sweep to update many
parameters indirectly. This is a powerful feature of KatScript, allowing for compact
descriptions of complex models.

Keywords
********

The following keywords exist in KatScript and serve special purposes. These do not need
to be specified in quotes when used as element or function arguments:

``none``
    Used to represent null.

``even``, ``odd``, ``x``, ``y``, ``off``
    Used to represent higher order mode sets.

``lin``, ``log``
    Used to represent scales.

``am``, ``pm``
    Used to represent modulation types.

``lowpass``, ``highpass``, ``bandpass``, ``bandstop``
    Used to represent filter types.

``div``, ``gouy``, ``l``, ``length``, ``loss``, ``finesse``, ``fsr``, ``fwhm``, ``pole``, ``modesep``, ``q``, ``g``, ``rc``, ``stability``, ``s``, ``resolution``, ``w``, ``w0``, ``z``, ``r``, ``tau``
    Cavity and beam properties.

Arrays
======

Some parameters, like the ``Rc`` parameter of a :kat:element:`mirror` or the
:kat:command:`modes` command, support arrays as inputs. Arrays in KatScript are defined
using comma-delimited lists of values contained within square brackets:

.. code-block:: katscript

    m m1 R=1 T=0 Rc=[1, 1.1]  # Radius of curvature in tangential (x) and sagittal (y) planes.

Arrays can also contain expressions:

.. code-block:: katscript

    m m1 R=1 T=0 Rc=[1, 1.1]
    m m2 R=1 T=0 Rc=[&m1.Rcx, 1.2]

Arrays can also contain other arrays. This can be useful for defining the modes to
simulate:

.. code-block:: katscript

    modes([[1, 0], [0, 1], [2, 0]])  # Simulate only these higher order modes.

Generating KatScript from a model
=================================

Once you've parsed a script into a model, you can continue to make changes using the
:ref:`python_api`. If you wish to then regenerate KatScript for the updated model, you
can use :meth:`.Model.unparse` or :meth:`.Model.unparse_file`. Note that this does not
(yet) preserve the original parse order.

.. jupyter-execute::

    from finesse import Model

    model = Model()

    # Parse a simple model.
    model.parse(
        """
        l l1 P=10
        s s1 l1.p1 m1.p1
        m m1 R=0.995 T=1-&m1.R
        s s2 m1.p2 m2.p1 L=10
        m m2 R=0.9999 T=1-&m2.R
        pd pd1 m2.p1.i
        pd pd2 m2.p2.o
        """
    )

    # Change the cavity length.
    model.spaces.s2.L = 9

    # Unparse the model.
    print(model.unparse())

.. warning::

    KatScript generation is an experimental feature and likely contains bugs. It is not
    (yet!) guaranteed to generate KatScript that parses back to the same model state.
    Use it with care!
