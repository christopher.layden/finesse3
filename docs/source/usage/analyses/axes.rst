.. include:: /defs.hrst

====
Axes
====

.. kat:analysis:: noxaxis

.. kat:analysis:: xaxis

    :See Also:

        :kat:analysis:`x2axis`, :kat:analysis:`x3axis`

.. kat:analysis:: x2axis

    :See Also:

        :kat:analysis:`xaxis`, :kat:analysis:`x3axis`

.. kat:analysis:: x3axis

    :See Also:

        :kat:analysis:`xaxis`, :kat:analysis:`x2axis`

.. kat:analysis:: sweep

.. kat:analysis:: freqresp
