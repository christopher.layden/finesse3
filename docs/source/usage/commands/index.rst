.. include:: /defs.hrst

.. _commands:

Commands
========

Commands set some properties of the model or its elements. Some commands may only be
specified once per model, others multiple times.

.. kat:command:: fsig

.. kat:command:: lambda

.. kat:command:: modes

.. kat:command:: link

    :See Also:

        :kat:element:`space`

.. kat:command:: intrix

.. kat:command:: tem
