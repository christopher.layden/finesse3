.. Finesse 3 documentation master file

.. only:: latex

    =======
    Finesse
    =======

.. only:: not latex

    .. include:: title.rst

    ----

.. toctree::
    :maxdepth: 1
    :name: sec-intro

    introduction/index

.. toctree::
    :maxdepth: 1
    :name: sec-usage

    getting_started/index
    usage/index
    physics/index
    developer/index

.. toctree::
    :maxdepth: 1
    :name: api

    api/index

.. toctree::
    :titlesonly:
    :maxdepth: 1

    :name: sec-appendix

    appendix.rst

.. toctree::
    :maxdepth: 1
    :name: sec-back

    zglossary/index
    indices.rst
