.. include:: /defs.hrst

.. _requirements_guide:

Requirements for building, running and developing Finesse
=========================================================

|Finesse| requires Python 3.8 or higher. Python 3.8 is a hard requirement due to the use
of positional-only arguments, walrus operators, the use of
:class:`functools.singledispatchmethod` and the requirement for ``dict`` to be
reversible in the code.

|Finesse| additionally requires one system requirement:

- SuiteSparse 4.0 or higher

SuiteSparse is only required to build |Finesse|, not to run it. That means users using a
built |Finesse| distribution (e.g. a Conda package or bdist wheel) do not need to
install it manually, but if someone tries to install a source distribution they will.

Finally, |Finesse| depends on various other Python packages to be able to build and run,
discussed in the next section.

Python requirements in Finesse
------------------------------

**Source code:** :source:`setup.py </setup.py>`, :source:`setup.cfg </setup.cfg>`,
:source:`pyproject.toml </pyproject.toml>`

|Finesse|'s Python requirements are specified following the approach defined by
:pep:`517` (see below). The build backend is, for simplicity, currently set to
``setuptools.build_meta``, though this can be changed later if a tool offering more
useful features becomes available.

The ``setuptools.build_meta`` backend requires runtime requirements, i.e. those required
to be installed in the environment for |Finesse| to run, to be listed in ``setup.cfg``
and/or ``setup.py`` files in the project root. It's not strictly necessary to use *both*
files with ``setuptools`` (indeed it's now recommended to use only the declarative
``setup.cfg`` where possible), but because |Finesse| needs Cython extensions with
relatively complex configurations to be built by the backend it uses a ``setup.py`` to
define the required logic. It may be possible to remove ``setup.py`` later (see
:issue:`367`).

.. rubric:: PEP 517 build backend specification

PEP 517 introduces a standard way for Python packages to specify the tool to use to
perform the actual building of the project (their so-called build *backend*) and the
packages required in order to perform the build (the build *requirements*). This
information is specified in a standardized way in a ``pyproject.toml`` file included in
the project root.

The de-facto backend has traditionally been the ``distutils`` or ``setuptools`` packages
provided with Python itself, and indeed ``setuptools`` is the default used by pip if a
``pyproject.toml`` file cannot be located in the project. One of the purposes of PEP 517
is to allow other backends to be more easily used.

Users or higher level package managers such as Conda can then use a build *frontend*
(such as `pip <https://pip.pypa.io/en/stable/>`__ or `poetry
<https://python-poetry.org/>`__) to actually perform the retrieval and installation of
the project and its requirements, invoking the build backends specified for the project
and its dependencies.

.. note::

   PEP 517 recommends (but does not strictly require) that build frontends by default
   set up an isolated environment in which the backend can build the project. This means
   that build requirements are installed in a temporary location used only for the
   building of the package, then deleted when the newly built package is moved to the
   user's environment and the temporary environment is removed. This behaviour is
   usually beneficial because it helps to catch missing dependencies and removes the
   need for build requirements (e.g. ``Cython``) to be available in the local
   environment, but when debugging build issues it can be useful to switch this
   behaviour off. In pip this can be done by adding the ``--no-build-isolation`` flag to
   ``pip install``. Note that you'll need to ensure the ``inplacebuild`` extras listed
   in ``pyproject.toml`` are installed.

Version pinning
~~~~~~~~~~~~~~~

The versions of the |Finesse| runtime requirements are set by a compromise between the
desire to use the latest (and usually greatest) versions and the need to help package
managers include |Finesse| alongside other tools with their own, possibly conflicting,
requirements. It is therefore best to set only minimum required versions in
``setup.cfg``, leaving the maximum version open unless there are known
incompatibilities.

We rely on the regular running of the tests in the continuous integration pipeline to
catch issues created by the latest versions of requirements. In cases where a particular
dependency version is identified to cause problems with |Finesse|, it can be forbidden
in the version string for the respective dependency in ``setup.cfg`` (see next section).
It is also useful to leave a note next to the requirement to inform others (including
package maintainers, who may not be |Finesse| developers) why this is the case.

Modifying requirements
~~~~~~~~~~~~~~~~~~~~~~

Modification of requirements involves editing one or more files:

- Runtime requirements are listed within the ``install_requires`` key of the
  ``[options]`` section of ``setup.cfg``.

- Build requirements are defined in the ``requires`` key within the ``[build-system]``
  section of ``pyproject.toml``.

- Extra requirements are defined in the ``[options.extras_require]`` section of
  ``setup.cfg`` and can be used to specify optional requirements for users or
  developers. These are not installed by most frontends by default. The
  ``extras_require`` section contains keys and values representing groups of
  requirements. For example, the key ``docs`` lists requirements for building the
  documentation.

:pep:`508` specifies the format for requirements strings.

.. warning::

   As described in a previous section, the *building* of |Finesse| usually takes place
   in an isolated environment, so most build requirements these can be pinned in
   ``pyproject.toml`` to whatever version is desired without causing trouble elsewhere.
   It is important, however, that at least the ``numpy`` and ``cython`` requirements are
   set to the same versions in ``pyproject.toml`` and ``setup.cfg`` because parts of the
   |Finesse| code are compiled and linked against the ``numpy`` C ABI. Using different
   versions of ``numpy`` or ``cython`` for building and running |Finesse| frequently
   results in numerous warnings issued by ``numpy``, or even errors. It's also very
   useful from the point of view of debugging to maintain binary compatibility between
   |Finesse| built in an isolated environment (i.e. via the full build process) and
   in-place (e.g. via ``make``).

After modifying requirements, verify that the tests still pass and build process still
operates as expected.
