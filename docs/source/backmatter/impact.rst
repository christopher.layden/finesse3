.. include:: /defs.hrst
.. _impact:

Impact
======

|Finesse| has been widely used in several projects; most dominantly by the gravitational
wave groups all over the world. This section provides an incomplete list of documents
citing the |Finesse| software, to document the wide and sustained impact the software
has on this research field.

.. image:: /images/finesse_downloads2014.png

Accumulated unique downloads of the |Finesse| binary over an example period.

.. cssclass:: nobg
.. figure:: /images/finesse_globe2014.jpg

Locations of |Finesse| downloads (program and manual) for the same period as shown
above.

If you are using |Finesse| for your work or research, we would like to know about it. We
also appreciate if you acknowledge |Finesse| in publications that make use of |Finesse|
results.

.. todo:: add preferred citation method

.. todo:: update list below



119. `Modeling circulating cavity fields using the discrete linear canonical transform`, 
     A.A. Ciobanu, D.D. Brown, P.J. Veitch, D.J. Ottaway (2021)

118. `Optimizing Gravitational-Wave Detector Design for Squeezed Light`.
     J.W. Richardson, S. Pandey, E. Bytyqi, T. Edo and R.X. Adhikari (2021)

117. `Point absorbers in Advanced LIGO <https://www.osapublishing.org/ao/abstract.cfm?uri=ao-60-13-4047>`__,
     Aidan F. Brooks et al. (2021)

116. `An Experiment for Observing Quantum Gravity Phenomena using Twin Table-Top 3D Interferometers
     <https://arxiv.org/abs/2008.04957>`__,  S. M. Vermeulen, L. Aiello, A. Ejlli, W. L. Griffiths, 
     A. L. James, K. L. Dooley, H. Grote (2021)

115. `Angular response of a triangular optical cavity analyzed by a linear approximation method 
     <https://arxiv.org/abs/2002.02703>`__, Satoshi Tanioka, Gui-guo Ge, Keiko Kokeyama, Masayuki Nakano, 
     Junegyu Park, Kiwamu Izumi (2020)

114. `Pykat: Python package for modelling precision optical interferometers
     <https://doi.org/10.1016/j.softx.2020.100613>`__, D.D. Brown, P. Jones, S. Rowlinson, 
     S. Leavey, A. Green, D. Toyra and A.Freise (2020)

113. `Practical test mass and suspension configuration for a cryogenic kilohertz gravitational wave 
     detector <https://doi.org/10.1103/PhysRevD.102.122003>`__, J. Eichholz, N. A. Holland, 
     V. B. Adya, J. V. van Heijningen, R. L. Ward, B. J. J. Slagmolen, D. E. McClelland, and 
     D. J. Ottaway, Phys. Rev. D 102, 122003 (2020)

112. `Simplified optical configuration for a sloshing-speedmeter-enhanced gravitational wave 
     detector <https://iopscience.iop.org/article/10.1088/1361-6382/ab5bb9>`__, A. Freise, H. Miao, 
     D.D. Brown (2019)

111. `Neutron Star Extreme Matter Observatory: A kilohertz-band gravitational-wave detector in the 
     global network <https://doi.org/10.1017/pasa.2020.39>`__, K. Ackley et al., 
     Publications of the Astronomical Society of Australia, 37, E047  (2020)

110. `Active sorting of orbital angular momentum states of light with a cascaded tunable resonator
     <https://doi.org/10.1038/s41377-020-0243-x>`__, Wei, S., Earl, S.K., Lin, J. et al. (2019)

109. `Influence of nonuniformity in sapphire substrates for a gravitational wave telescope
     <https://journals.aps.org/prd/abstract/10.1103/PhysRevD.100.082005>`__, 
     K. Somiya, E. Hirose, Y. Michimura, Physical Review D, Volume 100, Issue 8, id.082005 (2019)

108. `Multi-spatial-mode effects in squeezed-light-enhanced interferometric
     gravitational wave detectors <https://arxiv.org/abs/1704.08237>`__, D. Töyrä et
     al., Phys. Rev. D 96, 022006 (2017)

107. `Alignment sensing for optical cavities using radio-frequency jitter modulation
     <https://www.osapublishing.org/ao/abstract.cfm?uri=ao-56-13-3879>`__, P. Fulda et
     al., Applied Optics Vol. 56, Issue 13, pp. 3879-3888 (2017)

106. `The Holometer: an instrument to probe Planckian quantum geometry
     <http://iopscience.iop.org/article/10.1088/1361-6382/aa5e5c/meta>`__, Aaron Chou et
     al., Classical and Quantum Gravity, Volume 34, Number 6 (2017)

105. `Higher-order Laguerre–Gauss modes in (non-) planar four-mirror cavities for future
     gravitational wave detectors
     <https://www.osapublishing.org/ol/abstract.cfm?uri=ol-42-4-751>`__, A. Noack, C.
     Bogan, and B. Willke, Optics Letters Vol. 42, Issue 4, pp. 751-754 (2017)

104. `The influence of dual-recycling on parametric instabilities at Advanced LIGO
     <http://iopscience.iop.org/article/10.1088/1361-6382/aa8af8/meta>`__, A.G. Green et
     al., Classical and Quantum Gravity, Volume 34, Number 20 (2017)

103. `Broadband sensitivity enhancement of detuned dual-recycled Michelson
     interferometers with EPR entanglement <https://arxiv.org/abs/1704.07173>`__, D. D.
     Brown et al., Phys. Rev. D 96, 062003 (2017)

102. `Control of the gravitational wave interferometric detector Advanced Virgo
     <https://tel.archives-ouvertes.fr/tel-01625376/>`__, J. C. Diaz, PhD Thesis,
     Université Paris-Saclay (2017)

101. `Enhancing the sensitivity of future laser-interferometric gravitational wave
     detectors <http://theses.gla.ac.uk/7902/>`__, S.S. Leavey, PhD Thesis, University
     of Glasgow (2017)

100. `Interactions of light and mirrors: advanced techniques for modelling future gravitational wave detectors <http://etheses.bham.ac.uk/6500/>`__, 
     D. D. Brown, PhD Thesis, University of Birmingham (2016)

99. `Length Sensing and Control for AdLIGO
    <https://labcit.ligo.caltech.edu/~rana/docs/AdLIGO-LSC.pdf>`__ , Kentaro Somiya,
    Osamu Miyakawa, Peter Fritschel, and Rana Adhikali (2016)

98. `Searching for photon-sector Lorentz violation using gravitational-wave detectors
    <https://www.sciencedirect.com/science/article/pii/S037026931630421X>`__,
    V.A. Kostelecký, A.C. Melissinos, and M. Mewes, Physics Letters B, Volume 761, p. 1-7 
    (2016)

97. `Design study and prototype experiment of the KAGRA output mode-cleaner
    <http://iopscience.iop.org/article/10.1088/1742-6596/716/1/012032/meta>`__ , K
    Kazushiro Yano, Ayaka Kumeta and Kentaro Somiya (2016)

96. `Length sensing and control for Einstein Telescope Low Frequency
    <http://iopscience.iop.org/article/10.1088/1742-6596/716/1/012030/meta>`__ , V. Adya
    et al. (2016)

95. `Analytical calculation of Hermite-Gauss and Laguerre-Gauss modes on a bullseye
    photodiode <https://arxiv.org/abs/1606.01057>`__, Charlotte Bond, Paul Fulda and
    Andreas Freise (2016)

94. `Alignment sensing and control for squeezed vacuum states of light
    <http://arxiv.org/abs/arXiv:1507.06468>`__ , Emil Schreiber et al. (2015)

93. `Fast Simulation of Gaussian-Mode Scattering for Precision Interferometry
    <http://arxiv.org/abs/arXiv:1507.03806>`__ , Daniel Brown, Rory Smith and Andreas
    Freise (2015)

92. `Local-Oscillator Noise Coupling in Balanced Homodyne Readout for Advanced
    Gravitational Wave Detectors <http://arxiv.org/abs/arXiv:1506.07308>`__ , Sebastian
    Steinlechner et al. (2015)

91. `Upper limit to the transverse to longitudinal motion coupling of a waveguide mirror
    <http://arxiv.org/abs/1410.1808>`__, S Leavey, B W Barr, A S Bell, N Gordon, C Gräf,
    S Hild, S H Huttner, E-B Kley, S Kroker, J Macarthur, Classical and Quantum Gravity
    Volume 32 Number 17 (2015)

90. `Active wavefront control in and beyond Advanced LIGO
    <https://dcc.ligo.org/DocDB/0118/T1500188/004/mainAWC.pdf>`__, A. F. Brooks, R. X.
    Adhikari, S. Ballmer, L. Barsotti, P. Fulda, A. Perreca, LIGO technical note,
    LIGO-T1500188 (2015)

89. `In-situ characterization of the thermal state of resonant optical interferometers
    via tracking of their higher-order mode resonances
    <http://arxiv.org/abs/1502.02284>`__, Chris L. Mueller, Paul Fulda, Rana X.
    Adhikari, Koji Arai, Aidan F. Brooks, Rijuparna Chakraborty, Valery V. Frolov, Peter
    Fritschel, Eleanor J. King, David B. Tanner, Hiroaki Yamamoto, Guido Mueller,
    Classical and Quantum Gravity Volume 32 Number 13 (2015)

88. `Design study of the KAGRA output mode cleaner
    <http://link.springer.com/article/10.1007/s10043-015-0028-2>`__, Ayaka Kumeta,
    Charlotte Bond, Kentaro Somiya, Optical Review, Volume 22, Issue 1, pp 149-152
    (2015)

87. `Advanced Virgo: a second generation interferometric gravitational wave detector
    <http://arxiv.org/abs/1408.3978>`__, F. Acernese et al., Class. Quantum Grav. 32
    024001 (2015)

86. `How to stay in shape: overcoming beam and mirror distortions in advanced
    gravitational wave interferometers <http://etheses.bham.ac.uk/5223/>`__ Charlotte
    Zoë Bond, Ph.D. thesis, University of Birmingham (2014)

85. `Thermal correction of astigmatism in the gravitational wave observatory GEO 600
    <http://arxiv.org/abs/1311.5367>`__ H. Wittel et al., Class. Quantum Grav. 31 065008
    (2014)

84. `Sub nrad beam pointing monitoring and stabilization system for controlling input
    beam jitter in GW interferometers <http://arxiv.org/abs/1401.4981>`__ B. Canuel et
    al., Applied Optics, Vol. 53, Issue 13, pp. 2906-2916 (2014)

83. `Concepts and research for future detectors
    <http://link.springer.com/article/10.1007%2Fs10714-014-1700-8>`__, F. Acernese et
    al., General Relativity and Gravitation (2014)

82. `Analytical description of interference between two misaligned and mismatched
    complete Gaussian beams
    <http://www.opticsinfobase.org/ao/abstract.cfm?uri=ao-53-14-3043>`__, G. Wanner and
    G. Heinzel, Applied Optics, Vol. 53, Issue 14, pp. 3043-3048 (2014)

81. `Design of a speed meter interferometer proof-of-principle experiment
    <http://arxiv.org/abs/1405.2783>`__, C. Gräf et al., Class. Quantum Grav. 31 215009
    (2014)

80. `Design study of the KAGRA output mode-cleaner <http://arxiv.org/abs/1407.6080>`__,
    Ayaka Kumeta, Charlotte Bond, Kentaro Somiya (2014)

79. `Accelerated convergence method for fast Fourier transform simulation of coupled
    cavities <http://www.opticsinfobase.org/josaa/abstract.cfm?uri=josaa-31-3-652>`__,
    R. A. Day et al., Journal of the Optical Society of America A 31 652 (2014)

78. `Full modal simulation of opto-mechanical effects in optical systems
    <http://iopscience.iop.org/0264-9381/31/7/075005>`__, Gabriele Vajente, Classical
    and Quantum Gravity 31 075005 (2014)

77. `Comparing simulations of the Advanced LIGO Dual-Recycled Michelson
    <https://dcc.ligo.org/LIGO-T1400270/public>`__ C. Bond, P. Fulda, D. Brown and A.
    Freise, LIGO DCC: T1400270 (2014)

76. `Finesse input files for the L1 interferometer
    <https://dcc.ligo.org/LIGO-T1300901>`__, C. Bond, P. Fulda, D. Brown, K. Kokeyama,
    L. Carbone and A. Freise, LIGO DCC: T1300901 (2014)

75. `Finesse input files for the H1 interferometer
    <https://dcc.ligo.org/LIGO-T1300904>`__, C. Bond, P. Fulda, D. Brown, K. Kokeyama,
    L. Carbone, A.Perreca and A.Freise, LIGO DCC: T1300904 (2014)

74. `Simulations of effects of LLO mode-mismatches on PRFPMI error signals
    <https://dcc.ligo.org/LIGO-T1400182/public>`__, C. Bond, P. Fulda, D. Brown and A.
    Freise, LIGO DCC: T1400182 (2014)

73. `Comparing Finesse simulations, analytical solutions and OSCAR simulations of
    Fabry-Perot alignment signals <http://arxiv.org/abs/1401.5727>`__, S. Ballmer, J.
    Degallaix, A. Freise and P. Fulda, LIGO DCC: T1300345 and arXiv preprint
    arXiv:1401.5727, (2014)

72. `Optical Design and Numerical Modeling of the AEI 10m Prototype sub-SQL
    Interferometer <http://inspirehep.net/record/1345947/files/graef_thesis.pdf>`__,
    Christian Graef, PhD Thesis, University of Hannover (2013)

71. `Revisiting Sidebands of Sidebands in Finesse
    <https://dcc.ligo.org/LIGO-T1300986>`__, J. Clarke, H. Wang, D. Brown and A. Freise,
    LIGO DCC: T1300986 (2013)

70. `Interferometer responses to gravitational waves: Comparing FINESSE simulations and
    analytical solutions <http://arxiv.org/abs/1306.6752>`__, C, Bond, D. Brown, A.
    Freise (2013)

69. `Investigation of beam clipping in the Power Recycling Cavity of Advanced LIGO using
    FINESSE <https://dcc.ligo.org/LIGO-T1300954/public>`__ C. Bond, P. Fulda, D. Brown
    and A. Freise, LIGO DCC: T1300954 (2013)

68. `Finesse simulation for the alignment control signal of the aLIGO input mode cleaner
    <https://dcc.ligo.org/LIGO-T1300074/public>`__, K. Kokeyama, K. Arai, P. Fulda, S.
    Doravari, L. Carbone, D. Brown. C. Bond and A. Freise, LIGO DCC: T1300074 (2013)

67. `Report from the Commissioning Workshop at LLO
    <https://dcc.ligo.org/LIGO-T1300497>`__, K. Dooley et al., LIGO DCC: T1300497 (2013)

66. `Interferometer responses to gravitational waves: Comparing Finesse simulations and
    analytical solutions <http://arxiv.org/abs/1306.6752>`__, C. Bond, D. Brown and A.
    Freise, LIGO DCC: T1300190 and arXiv preprint arXiv:1306.6752 (2013)

65. `Phase effects due to beam misalignment on diffraction gratings
    <http://arxiv.org/abs/1303.7016>`__ Deepali Lodhia, Daniel Brown, Frank Brueckner,
    Ludovico Carbone, Paul Fulda, Keiko Kokeyama, Andreas Freise (2013)

64. `A realistic polarizing Sagnac topology with DC readout for the Einstein Telescope
    <http://arxiv.org/abs/1303.5236>`__ Mengyao Wang, Charlotte Bond, Daniel Brown,
    Frank Brueckner, Ludovico Carbone, Rebecca Palmer, Andreas Freise, Phys. Rev. D 87,
    096008 (2013)

63. `Generation of high-purity higher-order Laguerre-Gauss beams at high laser power
    <http://arxiv.org/abs/1303.3627>`__ L. Carbone, C. Bogan, P. Fulda, A. Freise, B.
    Willke, Phys. Rev. Lett., 110, 25 (2013)

62. `Length sensing and control of a Michelson interferometer with power recycling and
    twin signal recycling cavities <http://arxiv.org/abs/1211.7037>`__ Christian Graef
    et al, Optics Express, 21, 5287 (2013)

61. `Fast modal simulation of paraxial optical systems: the MIST open source toolbox
    <http://iopscience.iop.org/0264-9381/30/7/075014>`__ G. Vajente, Class. Quantum
    Grav. 30 075014 (2013)

60. `Experimental test of higher-order Laguerre–Gauss modes in the 10 m Glasgow
    prototype interferometer <http://iopscience.iop.org/0264-9381/30/3/035004>`__ B
    Sorazu, P J Fulda, B W Barr, A S Bell, C Bond, L Carbone, A Freise, S Hild, S H
    Huttner, J Macarthur and K A Strain, Class. Quantum Grav. 30 035004 (2013)

59. `Precision Interferometry in a New Shape: Higher-order Laguerre-Gauss Modes for
    Gravitational Wave Detection <http://etheses.bham.ac.uk/3703/>`__ Paul Fulda, Ph.D.
    thesis, University of Birmingham (2012)

58. `Status of the GEO 600 squeezed-light laser
    <http://iopscience.iop.org/1742-6596/363/1/012013/>`__ Khalaidovski, Alexander;
    Vahlbruch, Henning; Lastzka, Nico; Graef, Christian; Lueck, Harald; Danzmann,
    Karsten; Grote, Hartmut; Schnabel, Roman, Journal of Physics: Conference Series,
    Volume 363, Issue 1, pp. 012013 (2012)

57. `Advanced Virgo Technical Design Report
    <https://tds.ego-gw.it/itf/tds/index.php?callContent=2&callCode=9317>`__ Accadia et
    al. (The Virgo Collaboration), Virgo technical note VIR–0128A–12 (2012)

56. `Phase effects in Gaussian beams on diffraction gratings
    <http://iopscience.iop.org/1742-6596/363/1/012014>`__ D Lodhia, F Brueckner, L
    Carbone, P Fulda, K Kokeyama, A Freise, Journal of Physics: Conference Series, Vol
    363, number 1, pp 012014 (2012)

55. `The effect of mirror surface distortions on higher order Laguerre-Gauss modes
    <http://iopscience.iop.org/1742-6596/363/1/012005>`__ C Bond, P Fulda, L Carbone, K
    Kokeyama, A Freise, Journal of Physics: Conference Series, Vol 363, number 1, 012005
    (2012)

54. `Review of the Laguerre-Gauss mode technology research program at Birmingham
    <http://iopscience.iop.org/1742-6596/363/1/012010/>`__ Fulda, P.; Bond, C.; Brown,
    D.; Brückner, F.; Carbone, L.; Chelkowski, S.; Hild, S.; Kokeyama, K.; Wang, M.;
    Freise, A., Journal of Physics: Conference Series, Volume 363, Issue 1, pp. 012010
    (2012)

53. `The output mode cleaner of GEO 600
    <http://iopscience.iop.org/0264-9381/29/5/055009/>`__ Prijatelj, M.; Degallaix, J.;
    Grote, H.; Leong, J.; Affeldt, C.; Hild, S.; Lück, H.; Slutsky, J.; Wittel, H.;
    Strain, K.; Danzmann, K., Classical and Quantum Gravity, Volume 29, Issue 5, pp.
    055009 (2012)

52. `Einstein gravitational wave Telescope conceptual design study
    <http://www.et-gw.eu/etdsdocument/>`__ M Abernathy et al (ET Science Team), ET
    technical note ET-0106C-10 (2011)

51. `Eigenmode changes in a misaligned triangular optical cavity
    <http://iopscience.iop.org/2040-8978/13/5/055504>`__ F Kawazoe, R Schilling, H
    Lueck, Journal of Optics, Vol 13 055504 (2011)

50. `Diffractive gratings in high-precision interferometry for gravitational wave
    detection <http://etheses.bham.ac.uk/1459/>`__ Jonathan Hallam, Ph.D. thesis,
    University of Birmingham (2011)

49. `Higher order Laguerre-Gauss mode degeneracy in realistic, high finesse cavities
    <http://prd.aps.org/abstract/PRD/v84/i10/e102002>`__ C Bond, P Fulda, L Carbone, K
    Kokeyama, A Freise, Physical Review D, Vol 84, number 10, id 102002 (2011)

48. `Optical properties of 3-port-grating coupled cavities
    <http://edok01.tib.uni-hannover.de/edoks/e01dh10/617929971.pdf>`__ Oliver
    Burmeister, Ph. D. thesis, university of Hannover (2010)

47. `Experimental demonstration of higher-order Laguerre-Gauss mode interferometry
    <http://prd.aps.org/abstract/PRD/v82/i1/e012002>`__ Fulda, Paul; Kokeyama, Keiko;
    Chelkowski, Simon; Freise, Andreas, Physical Review D, vol. 82, Issue 1, id. 012002
    (2010)

46. Experimental demonstration of displacement noise free interferometry Antonio
    Perreca, Ph. D. Thesis, University of Birmingham (2010)

45. `Gravitational-wave detector-derived error signals for the LIGO thermal compensation
    system <http://iopscience.iop.org/0264-9381/27/21/215002>`__ R S Amin, J A Giaime,
    Classical and Quantum Gravity, Vol 27, pp 215002 (2010)

44. `Automatic Alignment for the first science run of the Virgo interferometer
    <http://www.sciencedirect.com/science/article/pii/S0927650510000290>`__ F Acernese
    et al., Astroparticle Physics, Vol 33, number 3, pp 131 (2010)

43. `Interferometer Techniques for Gravitational-Wave Detection
    <http://relativity.livingreviews.org/Articles/lrr-2010-1/>`__ A.Freise and K.Strain,
    Living Reviews in Relativity, 13 (2010)

42. `Modeling Thermal Phenomena and Searching for New Thermally Induced Monitor Signals
    in Large Scale Gravitational Wave Detectors
    <http://etd.lsu.edu/docs/available/etd-04222010-125635/>`__ R Amin, Ph. D. thesis,
    University of Florida (2010)

41. `Broadband squeezing of quantum noise in a Michelson interferometer with
    Twin-Signal-Recycling
    <http://www.opticsinfobase.org/abstract.cfm?URI=ol-34-6-824>`__ A Thuering, C Graef,
    H Vahlbruch, M Mehmet, K Danzmann, R Schnabel, Optics letters, Vol 34, number 6, pp
    824 (2009)

40. Virgo Input Mirrors thermal effects characterization, R. Day, V. Fafone, J. Marque,
    M. Pichot, M. Punturo, A. Rocchi, Virgo technical note VIR-0191A-10 (2010)

39. `DC-readout of a signal-recycled gravitational wave detector
    <http://iopscience.iop.org/0264-9381/26/5/055012>`__ S Hild, H Grote, J Degallaix, S
    Chelkowski, K Danzmann, A Freise, M Hewitson, J Hough, H Lueck, M Prijatelj, K A
    Strain, J R Smith, B Willke, Classical and Quantum Gravity, Vol 26, 055012 (2009)

38. `Prospects of higher-order Laguerre-Gauss modes in future gravitational wave
    detectors <http://prd.aps.org/abstract/PRD/v79/i12/e122002>`__ S Chelkowski, S Hild,
    A Freise, Physical Review D, Vol 79, issue 12, id 122002 (2009)

37. `Using the etalon effect for in situ balancing of the Advanced Virgo arm Cavities
    <http://iopscience.iop.org/0264-9381/26/2/025005/>`__ Hild, S.; Freise, A.;
    Mantovani, M.; Chelkowski, S.; Degallaix, J.; Schilling, R., Classical and Quantum
    Gravity, Volume 26, Issue 2, pp. 025005 (2009)

36. `On Special Optical Modes and Thermal Issues in Advanced Gravitational Wave
    Interferometric Detectors <http://www.livingreviews.org/lrr-2009-5%20>`__ Jean-Yves
    Vinet, Living Rev. Relativity 12, (2009)

35. `Coupling of lateral grating displacement to the output ports of a diffractive
    Fabry-Perot cavity <https://arxiv.org/abs/0903.3324>`__ J Hallam, S Chelkowski, A
    Freise, S Hild, B Barr, K A Strain, O Burmeister, R Schnabel, Journal of Optics A:
    Pure and Applied Optics, Volume 11, Issue 8, pp. 085502 (2009)

34. `Quantum noise and radiation pressure effects in high power optical interferometers
    <http://gwic.ligo.org/thesisprize/2008/Corbitt_Thesis.pdf>`__ T R Corbitt, Ph. D.
    Thesis, Massachusetts Institute of Technology (2008)

33. `Measurement and simulation of laser power noise in GEO 600
    <http://iopscience.iop.org/0264-9381/25/3/035003>`__ J R Smith, J Degallaix, A
    Freise, H Grote, M Hewitson, S Hild, H Lueck, K A Strain, B Willke, Classical and
    Quantum Gravity, Vol 25, pp 035003 (2008)

32. `Development of a signal-extraction scheme for resonant sideband extraction
    <http://iopscience.iop.org/0264-9381/25/23/235013/>`__ K.Kokeyama, K.Somiya,
    F.Kawazoe, S.Sato, S.Kawamura, and A.Sugamoto, Classical Quantum Gravity, Vol 25, pp
    235013 (2008)

31. `Experimental investigation of a control scheme for a zero-detuning resonant
    sideband extraction interferometer for next-generation gravitational-wave detectors
    <http://arxiv.org/abs/0804.4131>`__ F.Kawazoe, A.Sugamoto, V.Leonhardt, S.Sato,
    T.Yamazaki, M.Fukushima, S. Kawamura, O.Miyakawa, K.Somiya, T.Morioka, and
    A.Nishizawa, Classical Quantum Gravity, Vol 25, pp 195008 (2008)

30. `Demonstration of Displacement- and Frequency-Noise-Free Laser Interferometry Using
    Bidirectional Mach-Zehnder Interferometers
    <http://link.aps.org/doi/10.1103/PhysRevLett.98.141101>`__ S.Sato, K.Kokeyama,
    R.L.Ward, S.Kawamura, Y.Chen, A.Pai, and K.Somiya, Physics Review Letters, Vol 98,
    id 141101 (2007)

29. `Beyond the first Generation: Extending the Science Range of the Gravitational Wave
    Detector GEO 600. <http://www.amps.uni-hannover.de/dissertationen/hild_diss.pdf>`__
    Stefan Hild, Ph. D. thesis, University of Hannover (2007)

28. `Squeezed Light and Laser Interferometric Gravitational Wave Detectors
    <http://www.amps.uni-hannover.de/dissertationen/chelkowski_diss.pdf>`__ Simon
    Chelkowski, Ph. D. thesis, University of Hannover (2007)

27. `The GEO 600 core optics
    <http://www.sciencedirect.com/science/article/pii/S0030401807007377>`__ W Winkler et
    al., Optics Communications, Vol 280, number 2, pp 492 (2007)

26. `Measurement of the optical parameters of the Virgo interferometer
    <http://hal.archives-ouvertes.fr/in2p3-00113562/>`__ F Acernese et al., Applied
    Optics, Vol 46, pp 3466 (2007)

25. `Demonstration and comparison of tuned and detuned signal recycling in a large-scale
    gravitational wave detector <http://iopscience.iop.org/0264-9381/24/6/009>`__ S
    Hild, H Grote, M Hewitson, H Lueck, J R Smith, K A Strain, B Willke, K Danzmann,
    Classical and Quantum Gravity, Vol 24, pp 1513 (2007)

24. `A novel concept for increasing the peak sensitivity of LIGO by detuning the arm
    cavities <http://iopscience.iop.org/0264-9381/24/22/010>`__ S Hild, A Freise,
    Classical and Quantum Gravity, Vol 24, pp 5453 (2007)

23. `Optical modulation techniques for length sensing and control of optical cavities
    <http://www.opticsinfobase.org/abstract.cfm?&id=144538>`__ B W Barr, S H Huttner, J
    R Taylor, B Sorazu, M Plissi, K A Strain, Applied optics, Vol 46, number 31, pp 7739
    (2007)

22. `The Virgo automatic alignment system
    <http://iopscience.iop.org/0264-9381/23/8/S13>`__ F Acernese et al., Classical and
    Quantum Gravity, Vol 23, pp 91 (2006)

21. `Measurement of a low-absorption sample of OH-reduced fused silica
    <http://www.opticsinfobase.org/abstract.cfm?id=99586>`__ S Hild, H Lueck, W Winkler,
    K A Strain, H Grote, J Smith, M and Malec, M Hewitson, B Willke, J Hough, K
    Danzmann, Applied optics, Vol 45, number 28, pp. 7269 (2006)

20. `Des tests du Modele Standard a la recherche d'ondes gravitationnelles
    <http://hal.archives-ouvertes.fr/tel-00193159/>`__ E Tournefier, habilitation (2006)

19. `Lock Acquisition Scheme for the Advanced LIGO Optical Configuration
    <http://authors.library.caltech.edu/2686/1/MIYjpcs06.pdf>`__ O Miyakawa et al.,
    Journal of Physics: Conference Series, 32 265–269 (206)

18. `Control sideband generation for dual-recycled laser interferometric gravitational
    wave detectors <http://iopscience.iop.org/0264-9381/23/18/010>`__ B W Barr, O
    Miyakawa, S Kawamura, A J Weinstein, R Ward, S Vass, K A Strain, Classical and
    Quantum Gravity, Vol 23, pp 5661 (2006)

17. `Compensation of strong thermal lensing in advanced interferometric gravitational
    waves detectors
    <http://gravity.pd.uwa.edu.au/docs/PhDThesis/Jerome_thesis_low_res.pdf>`__ J.
    Degallaix, Ph. D. Thesis University of Western Australia (2006)

16. ` <http://deposit.ddb.de/cgi-bin/dokserv?idn=982491557&dok_var=d1&dok_ext=pdf&filename=982491557.pdf%0A>`__
    Formulation of instrument noise analysis techniques and their use in the
    commissioning of the gravitational wave observatory, GEO 600 JR Smith Ph. D. thesis,
    University of Hannover (2006)

15. `Diagonalizing sensing matrix of broadband RSE
    <http://iopscience.iop.org/1742-6596/32/1/072>`__ S.Sato, K.Kokeyama, F.Kawazoe,
    K.Somiya, and S.Kawamura, Journal of Physics Conference Series, Vol 32, pp 470
    (2006)

14. `Downselect of the signal extraction scheme for LCGT
    <http://iopscience.iop.org/1742-6596/32/1/065/>`__ K.Kokeyama, S.Sato, F.Kawazoe,
    K.Somiya, M.Fukushima, S.Kawamura, and A. Sugamoto, Journal of Physics Conference
    Series, Vol 32, pp 424 (2006)

13. `Commissioning of advanced, dual-recycled gravitational-wave detectors: simulations
    of complex optical systems guided by the phasor picture
    <http://pubman.mpdl.mpg.de/pubman/item/escidoc:150449:1>`__ M Malec, Ph. D. thesis,
    University of Hannover (2006)

12. `Mathematical framework for simulation of quantum fields in complex interferometers
    using the two-photon formalism <http://pra.aps.org/abstract/PRA/v72/i1/e013818>`__ T
    Corbitt, Y Chen, N Mavalvala, Physical Review A, vol. 72, Issue 1, id. 013818 (2005)

11. `Development of a frequency-detuned interferometer as a prototype experiment for
    next-generation gravitational-wave detectors
    <http://www.opticsinfobase.org/abstract.cfm?uri=ao-44-16-3179>`__ K.Somiya,
    P.Beyersdorf, K.Arai, S.Sato, S.Kawamura, O.Miyakawa, F. Kawazoe, S.Sakata,
    A.Sekido, and N.Mio, Applied Optics, Vol 44, pp 3179 (2005)

10. `Feedforward correction of mirror misalignment fluctuations for the GEO 600
    gravitational wave detector <http://iopscience.iop.org/0264-9381/22/14/018>`__ J R
    Smith, H Grote, M Hewitson, S Hild, H Lueck, M Parsons, K A Strain, B Willke,
    Classical and Quantum Gravity, Vol 22, pp 3093 (2005)

9. `The status of GEO 600 <http://iopscience.iop.org/0264-9381/22/10/009>`__ H Grote et
   al., Classical and Quantum Gravity, Vol 22, pp. 193 (2005)

8. `Towards dual recycling with the aid of time and frequency domain simulations
   <http://iopscience.iop.org/0264-9381/21/5/091>`__ M Malec, H Grote, A Freise, G
   Heinzel, K A Strain, J Hough, K Danzmann, Classical and Quantum Gravity, Vol 21, pp
   991 (2004)

7. `The status of GEO 600 <http://eprints.gla.ac.uk/4910/>`__ Strain, K.A. et al. In:
   Proceeding of SPIE: Gravitational Wave and Particle Astrophysics Detectors (2004)

6. `Dual recycling for GEO 600 <http://iopscience.iop.org/0264-9381/21/5/013>`__ H
   Grote, A Freise, M Malec, G Heinzel, B Willke, H Lueck, K A Strain, J Hough, K
   Danzmann, Classical and Quantum Gravity, Volume 21, pp 473 (2004)

5. `Characterization of the LIGO 4 km Fabry--Perot cavities via their high-frequency
   dynamic responses to length and laser frequency variations
   <http://iopscience.iop.org/0264-9381/21/5/015>`__ M Rakhmanov, F Bondu, O Debieu, R
   L Savage Jr, Classical and Quantum Gravity, Vol 21, pp 487 (2004)

4. `Thermal lensing compensation for AIGO high optical power test facility
   <http://iopscience.iop.org/0264-9381/21/5/079>`__ J Degallaix, C Zhao, L Ju, D
   Blair, Classical and Quantum Gravity, Vol 21, pp 903 (2004)

3. `Thermal correction of the radii of curvature of mirrors for GEO 600
   <http://iopscience.iop.org/0264-9381/21/5/090>`__ Lueck, H. Freise, A. Gossler, S.
   Hild, S. Kawabe, K. Danzmann, K., Classical and Quantum Gravity, Vol 21, pp 985
   (2004)

2. `The Next Generation of Interferometry: Multi-Frequency Optical Modelling, Control
   Concepts and Implementation <http://www.aei.mpg.de/pdf/doctoral/AFreise_03.pdf>`__
   Andreas Freise, Ph. D. thesis, University of Hannover (2003)

1. `Positions-und Orientierungsregelung von als Pendel aufgehaengten Resonatorspiegeln
   <http://www.aei.mpg.de/pdf/diploma/UWeiland_00.pdf>`__ U Weiland, Diploma Thesis,
   University of Hannover (2000)
