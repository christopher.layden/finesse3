import matplotlib.pyplot as plt
import finesse
from finesse.config import config_instance

CONFIG = config_instance()

finesse.configure(plotting=True)

kat = finesse.Model()
kat.parse(
    """
l l1 P=5000
s s1 l1.p1 m1.p1
m m1 R=0.99 T=0.01
s s2 m1.p2 m2.p2 L=1
m m2 R=1 T=0 phi=0

ligo_triple m1_sus m1.mech
ligo_triple m2_sus m2.mech

fsig 1

ad z1 m1.mech.z &fsig.f
ad z2 m2.mech.z &fsig.f

ad p1 m1.mech.pitch &fsig.f
ad p2 m2.mech.pitch &fsig.f

sgen sig m1.mech.F_z 1 0
xaxis sig.f log 0.3 0.6 200
"""
)

CONFIG["klu"]["rcond_diff_lim"] = "10000"
sol = kat.run()
plt.loglog(sol.x1, abs(sol["z1"]), label="Nothing")

CONFIG["klu"]["rcond_diff_lim"] = "0.1"
sol = kat.run()
plt.loglog(sol.x1, abs(sol["z1"]), label="Default 0.1 rcond diff lim")

CONFIG["klu"]["rcond_diff_lim"] = "10000000"
kat.fsig.f = 0.42
sol = kat.run()
plt.loglog(sol.x1, abs(sol["z1"]), label="Manual factor choice")

kat.l1.P = 0
sol = kat.run()
plt.loglog(sol.x1, abs(sol["z1"]), ls="--", label="Actual TF")
plt.legend(fontsize=8)

plt.xlabel("Frequency [Hz]")
plt.ylabel("PUM->TST Z [?]")
plt.show()
