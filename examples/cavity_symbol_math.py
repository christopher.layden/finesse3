# This example shows how you can use parameter references
# to make symbolic equations to compute some cavity parameters.
# You can use this

import finesse
import math

kat = finesse.Model()
kat.parse(
    """
m ITM R=0.99 T=0.01
s sCAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01
"""
)

# Define some math operations we'll need
sqrt = lambda x: finesse.element.Operation("sqrt", math.sqrt, x)
arcsin = lambda x: finesse.element.Operation("arcsin", math.asin, x)
pi = finesse.element.Constant(math.pi, name="π")
light_c = finesse.element.Constant(299792458, name="c")

# now we do some calculations
L_rt = kat.ITM.T.ref + kat.ETM.T.ref
L_cav = kat.spaces.sCAV.L.ref + kat.spaces.sCAV.L.ref
FSR = light_c / (L_cav)
r1 = sqrt(kat.ITM.R.ref)
r2 = sqrt(kat.ETM.R.ref)
FWHM = 2 * FSR / pi * arcsin((1 - r1 * r2) / (2 * sqrt(r1 * r2)))
Finesse = FSR / FWHM

print(f"Cavity length = {kat.spaces.sCAV.L}")
print(
    f"""
L_rt  = {L_rt.eval()}
L_cav = {L_cav.eval()}
FSR   = {FSR.eval()}
FWHM  = {FWHM.eval()}
"""
)


kat.spaces.sCAV.L = 2
print(f"Cavity length = {kat.spaces.sCAV.L}")
print(
    f"""
L_rt  = {L_rt.eval()}
L_cav = {L_cav.eval()}
FSR   = {FSR.eval()}
FWHM  = {FWHM.eval()}
"""
)

print("Printing the FWHM")
print(FWHM)
