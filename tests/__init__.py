"""Finesse tests."""

from faker import Faker

FAKER_SEED = 314159
Faker.seed(FAKER_SEED)
