""""""
import pytest
import finesse


@pytest.fixture
def dc_xysplit_model():
    model = finesse.Model()
    model.parse(
        """
    l l1
    bs bs1 R=1 T=0 ybeta=&bs1.xbeta
    nothing n1
    pd X n1.p1.i pdtype=xsplit
    pd Y n1.p1.i pdtype=ysplit
    link(l1, bs1, 10, n1)
    modes(maxtem=1)
    gauss g1 bs1.p2.o w0=1m z=0
    xaxis(bs1.xbeta, lin, -10u, 10u, 10)
    """
    )
    return model


def test_dc_xysplit(dc_xysplit_model):
    model = dc_xysplit_model
    model.run()
