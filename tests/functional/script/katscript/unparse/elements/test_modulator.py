import pytest
from finesse.components import Modulator
from finesse.enums import ModulatorType
from finesse.script import unparse
from finesse.script.generator import ElementContainer


names = pytest.mark.parametrize("name", ("mod", "mod1"))
frequencies = pytest.mark.parametrize("f", (1e6, 8.5e6))
indices = pytest.mark.parametrize("midx", (0.1, 0.3))
orders_and_types = pytest.mark.parametrize(
    "mod_type,order",
    (
        # AM only works with order 1.
        (ModulatorType.am, 1),
        (ModulatorType.pm, 1),
        (ModulatorType.pm, 2),
        (ModulatorType.pm, 3),
    ),
)
phases = pytest.mark.parametrize("phase", (0.0, 90.0))  # Floats!
positive_only = pytest.mark.parametrize("positive_only", (True, False))


@names
@frequencies
@indices
@orders_and_types
@phases
@positive_only
def test_modulator(model, name, f, midx, order, mod_type, phase, positive_only):
    model.add(
        Modulator(
            name,
            f=f,
            midx=midx,
            order=order,
            mod_type=mod_type,
            phase=phase,
            positive_only=positive_only,
        )
    )

    # The modulator needs wrapped in a :class:`.ElementContainer` to make it unparse.
    script = unparse(ElementContainer(getattr(model, name)))
    expected = (
        f"modulator {name} f={f} midx={midx} order={order} "
        f"mod_type={unparse(mod_type)} phase={phase} "
        f"positive_only={unparse(positive_only)}"
    )
    assert script == expected
